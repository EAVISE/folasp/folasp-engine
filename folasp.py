import logging
import sys
import ply.yacc as yacc
import ply.lex as lex
import re
import os
import copy
import argparse
from collections import OrderedDict  # , namedtuple
from functools import reduce


DEBUG = False

############################################################
#   PARSER                                                 #
############################################################

block_names = {'VOCAB': 'vocabulary', 'STRUC': 'structure', 'THEOR': 'theory', 'OPTIMIZE': 'term'}


#
#   INTERNAL REPRESENTATION
#

def extract_name(s):
    if s is None:
        return
    if isinstance(s, str):
        return s
    if isinstance(s, int):
        return str(s)
    return extract_name(s.children[0])


class Node(object):
    """ A Node object consists of a node symbol and a list of children, which are in turn Nodes (or strings objects).
        These nodes are used to build a tree representation of a FOL program.
        e.g., Node(AND, [Node(OR, [Node(NAME,['x']), Node(NAME,['y'])]), Node(NAME,['z'])]) == (x|y)&z
    """

    def __init__(self, symb, ch=None):
        if not isinstance(symb, str):
            raise TypeError(f'{str(symb)} is not a string')
        if ch and not isinstance(ch, list):
            raise TypeError(f'{str(ch)} is not a list')
        self.symbol = symbol_names[symb] if symb in list(symbol_names.keys()) else symb
        self.children = ch if ch else []

    def __hash__(self):
        return hash(self.symbol) ^ reduce(lambda x, y: x ^ y, list(map(hash, self.children)))

    def __eq__(self, other):
        if self.symbol != other.symbol or len(self.children) != len(other.children):
            return False
        return all(self.children[i] == other.children[i] for i in range(len(self.children)))

    def __str__(self):
        """ SYMBOL[<children>]
            e.g., AND[OR[NAME[x], NAME[y]], NAME[z]] == (x|y)&z
        """
        return self.symbol + "[" + ','.join(map(str, self.children)) + "]"

    def print(self, print_depth=2):
        """ Print parse tree of Node with a given depth. """
        def recurse_print(node, depth=0, indent=0):
            try:
                if depth == indent:
                    print('\t' * indent + str(node))
                    return
                if node.symbol in ['NAME', 'ARITY', 'TYPING']:
                    print('\t' * indent + str(node))  # .symbol) + '[' + str(node.children[0]) + ']'
                    return
                print('\t' * indent + str(node.symbol) + '[')
                for x in node.children:
                    if isinstance(x, list):
                        for y in x:
                            recurse_print(y, depth=depth, indent=indent + 1)
                    else:
                        recurse_print(x, depth=depth, indent=indent + 1)
                print('\t' * indent + ']')
            except AttributeError:
                print('\t' * indent + str(node))
                pass
            return
        recurse_print(self, print_depth)

    def receive(self, visitor, *args):
        func = "visit_" + self.symbol
        return getattr(visitor, func)(self, *args) 

    def get_name(self):
        return extract_name(self)

    def set_name(self, s):
        if self.symbol != 'NAME':
            raise AttributeError('Can only set name of a NAME node')
        self.children[0] = str(s)
        return

    @staticmethod
    def Name(name):
        """ Return new name node. NAME[<name>] """
        return Node('NAME', [name])

    @staticmethod
    def Bool(bool_=True):
        """ Return new boolean node. BOOL[<TRUE/FALSE>] """
        x = bool_
        if isinstance(bool_, str):
            x = str(bool_).upper()
        if isinstance(bool_, int):
            x = 'TRUE' if bool_ == 1 else ('FALSE' if bool_ == 0 else 'NONE')

        if x not in ('TRUE', 'FALSE'):
            raise ValueError(f'"{bool_}" is not a boolean value.')
        return Node('BOOL', [x])

    @staticmethod
    def EmptyBlock(kind, name, vocname=None):
        """ Return new block node. <KIND>[NAME[<name>], (NAME[<vocname>]), BODY[]]
            e.g., STRUCTURE[NAME[<S>], NAME[<V>], BODY[]])
        """
        if kind not in list(block_names.values()):
            raise ValueError(f"'{kind}' is not a valid IDP block. "
                             + "If it is a 'query' or 'procedure' block, these are not parsed yet.")
        return _parse_block(kind + ' ' + name + (':' + vocname if vocname else '') + '{}')


class Symbol(object):
    def __init__(self, node):
        """ Create Symbol with symbol kind, name and typing or arity info (if given) from a Node.
            (Assuming a symbol node has at least two children: first it's name and second its arity or typing.)
        """
        self.symbol = node.symbol
        self.name = extract_name(node.children[0])
        if len(node.children) == 1:
            return
        info = node.children[1]
        if info.symbol == 'TYPING':
            self.typing = info.children
            self.arity = len(self.typing)
        elif info.symbol == 'ARITY':
            self.arity = info.children[0]
            if len(node.children) > 2 and node.children[2].symbol == 'BUILTINTYPE':
                self.builtintype = extract_name(node.children[2])

    def __hash__(self):
        x = (self.symbol, self.name)
        try:
            x += tuple(self.typing)
        except AttributeError:
            try:
                x += tuple([self.arity])
            except AttributeError:
                pass
        return hash(x)

    def __eq__(self, other):
        if not isinstance(other, type(self)):
            return NotImplemented
        return self.matches(other)

    def __str__(self):
        s = self.name
        try:
            s += '/' + str(self.arity)
            s += '[' + ','.join([extract_name(x) for x in self.typing[:self.arity]]) + ']'
            s += '(' + self.builtintype + ')'
        except AttributeError:
            pass
        return s

    def copy(self, name=None):
        """ Copy a symbol object to a new one, with optionally a new name."""
        symbol = self.__class__(self.to_node())
        if name:
            symbol.name = name
        return symbol

    def matches(self, other):
        """ Check if Symbol matches another one. i.e., Same symbol*, name, arity and typing as given other Symbol.
            *partial and complete functions are considered the same.
        """
        if self.symbol != other.symbol:
            if not (isinstance(self, FunctionSymbol) and isinstance(other, FunctionSymbol)):  # exception: partial funcs
                return False
            logging.warning('Trying to match function and partial function')
        if self.name != other.name:
            return False
        try:
            if self.arity != other.arity:
                return False
            if self.typing != other.typing:
                return False
            return True
        except AttributeError:
            # logging.warning('Could not compare (all) symbol attributes, possibly incorrect match if incomplete symbols were provided')
            return True

    def find_in(self, symb_list):
        """ Find Symbol in a list of Symbols. Raise error if not found or ambiguous. """
        matches = [x for x in symb_list if self.matches(x)]
        if len(matches) < 1:
            raise ValueError(f'{str(self)} not found')
        if len(matches) > 1:
            t = '", "'.join(map(str, matches[:-1]))
            raise ValueError(f'Ambiguity in identity of "{str(self)}": could be "{t}" or "{matches[-1]}"')
        return matches[0]

    def to_node(self):
        """ Convert Symbol object to Node object. """
        nodes = [Node.Name(self.name)]
        try:
            nodes.append(Node('TYPING', self.typing))
        except AttributeError:
            try:
                nodes.append(Node('ARITY', [self.arity]))
            except AttributeError:
                pass
        try:
            nodes.append(Node('BUILTINTYPE', [Node.Name(self.builtintype)]))
        except AttributeError:
            pass
        return Node(self.symbol, nodes)

    @staticmethod
    def node(kind, name_node, arity=None, typing=None, builtintype=None):
        """ Return a new Node for a symbol. """
        try:
            if name_node.symbol != 'NAME':
                raise ValueError(f'Expected a \'NAME\' node here but got a \'{name_node.symbol}\' node instead.')
        except AttributeError:
            raise AttributeError(f'Expected a Node object but got \'{name_node}\' instead.')
        nodes = [name_node]
        if typing is not None:
            nodes.append(Node('TYPING', typing))
        elif arity is not None:
            nodes.append(Node('ARITY', [arity]))
        if builtintype:
            nodes.append(Node('BUILTINTYPE', [builtintype]))
        return Node(kind, nodes)


class PredicateSymbol(Symbol):
    def __init__(self, node):
        if node.symbol != 'PRED':
            raise ValueError(f'{str(node)} is not a predicate')
        super(PredicateSymbol, self).__init__(node)

    def is_type(self):
        try:
            self.arity
        except AttributeError:
            raise Exception("Cannot determine if symbol is a type.")
        if self.arity != 1:
            return False
        try:
            self.typing
            return False
        except AttributeError:
            pass
        return True

    def is_bool(self):
        try:
            if self.arity == 0:
                return True
        except AttributeError:
            raise Exception("Cannot determine if symbol is a boolean.")
        return False

    @staticmethod
    def typeSymbol(name_node):
        """ Return new Node for a type, i.e., unary predicate symbol. """
        return PredicateSymbol(PredicateSymbol.node(name_node, arity=1))

    @staticmethod
    def node(name_node, arity=None, typing=None, builtintype=None):
        """ Return a new Node object for a predicate symbol. """
        if typing is not None:
            arity = None
        return Symbol.node('PRED', name_node, arity=arity, typing=typing, builtintype=builtintype)


class FunctionSymbol(Symbol):
    def __init__(self, node):
        """ Create FunctionSymbol as a Symbol with symbol kind 'FUNC' or 'PFUNC' (for partial). """
        if node.symbol != 'FUNC' and node.symbol != 'PFUNC':
            raise ValueError(f'{str(node)} is not a function')
        super(FunctionSymbol, self).__init__(node)
        try:  # Arity should be decremented because of typing info of function return type.
            self.arity = len(self.typing)-1
        except AttributeError:
            pass

    def __str__(self):
        s = ('*' if self.symbol == 'PFUNC' else '') + super(FunctionSymbol, self).__str__()
        try:
            self.arity
            s += ':1'
            s += '[' + extract_name(self.typing[-1]) + ']'
        except AttributeError:
            pass
        return s

    def is_partial(self):
        if self.symbol == 'PFUNC':
            return True
        return False

    def return_type(self):
        """ Return the function symbol's return type. """
        if not hasattr(self, 'typing'):
            raise AttributeError(f'No typing available for function symbol {self.name}')
        return self.typing[-1]

    def to_pred(self):
        """ Convert function symbol to predicate symbol (by changing symbol name and altering arity, if given). """
        try:
            self.arity += 1
        except AttributeError:
            pass
        self.__class__ = PredicateSymbol
        return self

    @staticmethod
    def node(name_node, is_partial=False, arity=None, typing=None):
        """ Return a new Node object for a function symbol. """
        if typing is not None:
            arity = None
        if is_partial:
            return Symbol.node('PFUNC', name_node, arity, typing)
        return Symbol.node('FUNC', name_node, arity, typing)


#
#   LEX TOKENIZER
#

class LexError(Exception):
    pass


reserved = {
    'type': 'TYPE',
    'partial': 'PARTIAL',
    'isa': 'SUB',
    'contains': 'SUPER',
    'constructed': 'CONSTR',
    'from': 'FROM',
    'nat': 'NAT',
    'int': 'INT',
    'string': 'STR',

    'ct': 'CERTAINLYTRUE',
    'cf': 'CERTAINLYFALSE',
    'true': 'TRUE',
    'false': 'FALSE',

    'card': 'CARD',
    'sum': 'SUM',
    'prod': 'PROD',
    'min': 'MIN',
    'max': 'MAX',

    'abs': 'ABS',
    'MAX': 'TYPEMAX',
    'MIN': 'TYPEMIN',
}

tokens = ['VOCAB', 'STRUC', 'THEORY', 'TERM',
          'AND', 'OR', 'NOT', 'RIMP', 'LIMP', 'EQUIV',
          'FORALL', 'EXISTS', 'DEFIMPL', 'MAP', 'ID',
          'LPAR', 'RPAR', 'LBRACE', 'RBRACE', 'LSQBRACK', 'RSQBRACK',
          'DOT', 'COMMA', 'COLON', 'SEMICOLON', 'QUOTE', #'APOST'
          'EQ', 'LEQ', 'GEQ', 'NEQ', 'LT', 'GT',
          'PLUS', 'MINUS', 'TIMES', 'DIVIDE', 'MODULO',
          'COUNT'] + list(reserved.values())

t_AND = r'&'
t_OR = r'\|'
t_NOT = r'~'
t_RIMP = r'=>'
t_LIMP = r'<='
t_EQUIV = r'<=>'
t_FORALL = r'!'
t_EXISTS = r'\?'
t_DEFIMPL = r'<-'
t_MAP = r'->'

t_LPAR = r'\('
t_RPAR = r'\)'
t_LBRACE = r'\{'
t_RBRACE = r'\}'
t_LSQBRACK = r'\['
t_RSQBRACK = r'\]'
t_DOT = r'\.'
t_COMMA = r','
t_COLON = r':'
t_SEMICOLON = r';'
#t_APOST = r'\''
t_QUOTE = r'\"'

t_EQ = r'='
t_NEQ = r'~='
t_LT = r'<'
t_LEQ = r'=<'
t_GT = r'>'
t_GEQ = r'>='
t_PLUS = r'\+'
t_MINUS = r'-'
t_TIMES = r'\*'
t_DIVIDE = r'/'
t_MODULO = r'\%'
t_COUNT = r'\#'

t_ignore = " \t\n\r"


def t_VOCAB(t):
    r'(?i)vocabulary'
    t.value = t.value.upper()
    return t


def t_STRUC(t):
    r'(?i)structure'
    t.value = t.value.upper()
    return t


def t_THEORY(t):
    r'(?i)theory'
    t.value = t.value.upper()
    return t


def t_TERM(t):
    r'(?i)term'
    t.value = t.value.upper()
    return t


def t_ID(t):
    r"[a-zA-Z0-9_']+"
    t.type = reserved.get(t.value, 'ID')  # Check for reserved words
    return t


def t_error(t):
    raise LexError(t)


#
#   YACC GRAMMAR
#

class YaccError(Exception):
    pass


precedence = (
    ('left', 'COMMA'),
    ('nonassoc', 'FORALL', 'EXISTS'),
    ('nonassoc', 'DEFIMPL'),
    ('nonassoc', 'EQUIV'),
    ('nonassoc', 'RIMP', 'LIMP'),
    ('left', 'OR'),
    ('left', 'AND'),
    ('right', 'NOT'),
    ('nonassoc', 'LT', 'GT', 'LEQ', 'GEQ'),
    ('left', 'PLUS', 'MINUS'),
    ('left', 'TIMES', 'DIVIDE', 'MODULO'),
    ('right', 'UMINUS'),
)

symbol_names = {'~': 'NOT', '&': 'AND', '|': 'OR', '!': 'FORALL', '?': 'EXISTS',
                '=>': 'LIMP', '<=': 'RIMP', '<=>': 'EQUIV',
                '=': 'EQ', '~=': 'NEQ', '<': 'LT', '=<': 'LEQ', '>': 'GT', '>=': 'GEQ',
                '+': 'PLUS', '-': 'MINUS', '*': 'TIMES', '/': 'DIVIDE', '%': 'MODULO', }


# idp block

def p_idp_block_list_many(p):
    'idp_block_list : idp_block_list idp_block'
    p[0] = p[1] + p[2]


def p_idp_block_list_one(p):
    'idp_block_list : idp_block'
    p[0] = p[1]


def p_idp_block(p):
    '''idp_block : vocab_block
                 | struc_block
                 | theory_block
                 | term_block'''
    if not isinstance(p[1], list):
        p[1] = [p[1]]
    p[0] = p[1]


# VOCABULARY

def p_vocab_block(p):
    'vocab_block : VOCAB name LBRACE vocab_body RBRACE'
    p[0] = Vocabulary(Node(p[1], [p[2], Node('NAME', ['']), Node('VOCBODY', p[4])]))  # [0])]))


def p_vocab_block_empty(p):
    'vocab_block : VOCAB name LBRACE RBRACE'
    p[0] = Vocabulary(Node(p[1], [p[2], Node('NAME', ['']), Node('VOCBODY', [])]))


def p_vocab_body_many(p):
    'vocab_body : vocab_body declaration'
    p[0] = p[1] + [p[2]]


def p_vocab_body_one(p):
    'vocab_body : declaration'
    p[0] = [p[1]]


def p_declaration(p):
    '''declaration : type_decl
                   | pred_decl
                   | func_decl '''
    if not isinstance(p[1], list):
        p[1] = [p[1]]
    p[0] = Node('DECLARATION', p[1])


# Type declaration

def p_type_decl(p):  # 'type T'
    'type_decl : TYPE name'
    p[0] = PredicateSymbol.node(p[2], arity=1)


def p_type_decl_inherit(p):  # 'type T isa T1,T2,...' / 'type T contains T1,...'
    '''type_decl : type_decl SUB element_list
                 | type_decl SUPER element_list'''
    logging.warning(f'Sub- and super types are not supported, make sure {extract_name(p[1])} is interpreted as well as ' +
                    'its super- or sub type.')
    p[0] = p[1]


def p_type_decl_builtin(p):  # 'type T isa int / 'type T isa nat'
    '''type_decl : type_decl SUB INT
                 | type_decl SUB NAT
                 | type_decl SUB STR'''
    if not isinstance(p[1], list):
        p[1] = [p[1]]
    p[1][0].children.append(Node("BUILTINTYPE", [Node("NAME", [p[3]])]))
    p[0] = p[1]


def p_type_decl_construct(p):  # 'type T constructed from { 'a', F(T,T), C }'
    'type_decl : TYPE name CONSTR FROM LBRACE type_enum RBRACE'
    type = PredicateSymbol.node(p[2], arity=1)
    p[0] = [type, p[6]]


def p_type_decl_numerical(p):  # 'type T = { 1; 2; ... }'
    'type_decl : TYPE name EQ pred_enum'  # LBRACE tuple_list RBRACE'
    type = PredicateSymbol.node(p[2], arity=1)
    p[0] = [type, p[4]]


def p_type_enum(p):
    'type_enum : element_list'
    p[0] = Node('PRED_ENUM', p[1])


# Predicate declaration

def p_pred_decl(p):  # 'P(T,T,...)'
    'pred_decl : name LPAR element_list RPAR'
    p[0] = PredicateSymbol.node(p[1], typing=p[3])


def p_pred_decl_bool(p):  # 'B' / 'B()'
    '''pred_decl : name
                 | name LPAR RPAR'''
    p[0] = PredicateSymbol.node(p[1], typing=[])


# Function declaration

def p_func_decl(p):  # 'F(T,T,...):T'
    'func_decl : name LPAR element_list RPAR COLON name'
    p[3].append(p[6])
    p[0] = FunctionSymbol.node(p[1], typing=p[3])


def p_func_decl_part(p):  # 'partial F(T,T,...):T'
    'func_decl : PARTIAL func_decl'
    p[2].symbol = 'PFUNC'
    p[0] = p[2]


def p_func_decl_const(p):  # 'C:T'
    'func_decl : name COLON name'
    p[0] = FunctionSymbol.node(p[1], typing=[p[3]])


def p_func_decl_const_par(p):  # 'C():T'
    'func_decl : name LPAR RPAR COLON name'
    p[0] = FunctionSymbol.node(p[1], typing=[p[5]])


# STRUCTURE

def p_struc_block(p):
    'struc_block : STRUC name COLON name LBRACE struc_body RBRACE'
    p[0] = Structure(Node('STRUCTURE', [p[2], p[4], Node('STRUCBODY', p[6])]))


def p_struc_block_empty(p):
    'struc_block : STRUC name COLON name LBRACE RBRACE'
    p[0] = Structure(Node('STRUCTURE', [p[2], p[4], Node('STRUCBODY', [])]))


def p_struc_body_many(p):
    'struc_body : struc_body assignment'
    p[0] = p[1] + [p[2]]


def p_struc_body_one(p):
    'struc_body : assignment'
    p[0] = [p[1]]


def p_assignment(p):
    '''assignment : pred_assign
                  | func_assign'''
    p[0] = Node('ASSIGNMENT', p[1])


# Predicate assignment

def p_pred_assign(p):  # 'P = ...'
    'pred_assign : name EQ pred_eq'
    p[0] = [PredicateSymbol.node(p[1]), p[3], Node('NVALUED', [None])]


def p_pred_assign_args(p):  # 'P[T,T,...] = ...'
    'pred_assign : name pred_typing EQ pred_eq'
    p[0] = [PredicateSymbol.node(p[1], typing=p[2]), p[4], Node('NVALUED', [None])]


def p_pred_assign_three_valued(p):  # 'P<cf> = ...'
    'pred_assign : name three_valued EQ pred_eq'
    p[0] = [PredicateSymbol.node(p[1]), p[4], Node('NVALUED', [p[2]])]


def p_pred_assign_three_valued_args(p):  # 'P[T,T,...]<cf> = ...'
    'pred_assign : name pred_typing three_valued EQ pred_eq'
    p[0] = [PredicateSymbol.node(p[1], typing=p[2]), p[5], Node('NVALUED', [p[3]])]


def p_pred_typing(p):  # '[T,T,T]'
    'pred_typing : LSQBRACK element_list RSQBRACK'
    p[0] = p[2]


def p_pred_typing_bool(p):  # '[]'
    'pred_typing : LSQBRACK RSQBRACK'
    p[0] = []


def p_three_valued_true(p):  # '<ct>'
    'three_valued : LT CERTAINLYTRUE GT'
    p[0] = True


def p_three_valued_false(p):  # '<cf>'
    'three_valued : LT CERTAINLYFALSE GT'
    p[0] = False


# Predicate interpretation

def p_pred_eq(p):
    '''pred_eq : pred_enum
               | pred_bool'''
    p[0] = p[1]


def p_pred_enum(p):  # '{ 1,a; (2,b) }'
    'pred_enum : LBRACE tuple_list RBRACE'
    p[0] = Node('PRED_ENUM', p[2])


def p_pred_bool_false(p):  # 'false' / '{}'
    '''pred_bool : FALSE
                 | LBRACE RBRACE'''
    # if p[1] == 'LBRACE': logging.warning('Ambiguity in assignment of empty enumeration (could be empty or boolean)')
    p[0] = Node('PRED_ENUM', [])


def p_pred_bool_true(p):  # 'true' / '{()}'
    '''pred_bool : TRUE
                 | LBRACE LPAR RPAR RBRACE'''
    p[0] = Node('PRED_ENUM', [Node('TUPLE', [])])


# Function assignment

def p_func_assign(p):  # 'F = ...'
    'func_assign : name EQ func_eq'
    p[0] = [FunctionSymbol.node(p[1]), p[3], Node('NVALUED', [None])]


def p_func_assign_args(p):  # 'F[T,T,...:T] = ...'
    'func_assign : name func_typing EQ func_eq'
    p[0] = [FunctionSymbol.node(p[1], typing=p[2]), p[4], Node('NVALUED', [None])]


def p_func_assign_three_valued(p):  # 'F<cf> = ...'
    'func_assign : name three_valued EQ func_eq'
    p[0] = [FunctionSymbol.node(p[1]), p[4], Node('NVALUED', [p[2]])]


def p_func_assign_three_valued_args(p):  # 'F[T,T,...:T]<cf> = ...'
    'func_assign : name func_typing three_valued EQ func_eq'
    p[0] = [FunctionSymbol.node(p[1], typing=p[2]), p[5], Node('NVALUED', [p[3]])]


def p_func_typing_func(p):  # '[T,T:T]'
    'func_typing : LSQBRACK element_list COLON name RSQBRACK'
    p[0] = p[2] + [p[4]]


def p_func_typing_const(p):  # '[:T]'
    'func_typing : LSQBRACK COLON name RSQBRACK'
    p[0] = [p[4]]


# Function interpretation

def p_func_eq(p):
    '''func_eq : func_enum
               | func_const'''
    p[0] = p[1]


def p_func_enum(p):  # '{ 1,a->b; (2,b)->c }'
    'func_enum : LBRACE mapping_list RBRACE'
    p[0] = Node('FUNC_ENUM', p[2])


def p_mapping_list_many(p):
    'mapping_list : mapping_list SEMICOLON mapping'
    p[0] = p[1] + [p[3]]


def p_mapping_list_one(p):
    'mapping_list : mapping'
    p[0] = [p[1]]


def p_mapping(p):
    'mapping : tuple MAP name'
    p[1].children.append(p[3])
    p[0] = p[1]


def p_func_const_long(p):  # '{ -> a }'
    'func_const : LBRACE MAP name RBRACE'
    p[0] = Node('FUNC_ENUM', [Node('TUPLE', [p[3]])])


def p_func_const(p):  # 'a' /  ''a'' / '"a"'
    'func_const : name'
    p[0] = Node('FUNC_ENUM', [Node('TUPLE', [p[1]])])


# TERM

def p_term_block(p):
    'term_block : TERM name COLON name LBRACE term_body RBRACE'
    p[0] = Term(Node('OPTIMIZE', [p[2], p[4], Node('OPBODY', p[6])]))


def p_term_body(p):
    'term_body : term'
    p[0] = [p[1]]


# THEORY

def p_theory_block(p):
    'theory_block : THEORY name COLON name LBRACE theory_body RBRACE'
    p[0] = Theory(Node('THEORY', [p[2], p[4], Node('THBODY', p[6])]))


def p_theory_block_empty(p):
    'theory_block : THEORY name COLON name LBRACE RBRACE'
    p[0] = Theory(Node('THEORY', [p[2], p[4], Node('THBODY', [])]))


def p_theory_body_many(p):
    'theory_body : theory_body constraint'
    p[0] = p[1] + [p[2]]


def p_theory_body_one(p):
    'theory_body : constraint'
    p[0] = [p[1]]


def p_constraint(p):  # 'f.'
    'constraint : formula DOT'
    p[0] = Node('CONSTRAINT', [p[1]])


def p_constraint_definition(p):  # '{ <definition> }'
    'constraint : definition'
    p[0] = p[1]


# Formula
#  (formula between parentheses, negation of formula, binary op. of formulas, quantification of formula, atom)

def p_formula_par(p):  # '(f)'
    'formula : LPAR formula RPAR'
    p[0] = p[2]


def p_formula_neg(p):  # '~f'
    'formula : NOT formula'
    p[0] = Node('NOT', [p[2]])


def p_formula_binop(p):  # 'f & g' / 'f | g' / 'f=>g' / ...
    '''formula : formula AND formula
               | formula OR formula
               | formula LIMP formula
               | formula RIMP formula
               | formula EQUIV formula'''
    if p[1].symbol == symbol_names[p[2]] and p[2] in ('AND', 'OR'):
        p[1].children.append(p[3])
        p[0] = p[1]
    else:
        p[0] = Node(p[2], [p[1], p[3]])


def p_formula_quantified(p):  # '!x[T] y[T]: f' / '?x[T] y[T]: f'
    '''formula : FORALL var_list COLON formula %prec FORALL
               | EXISTS var_list COLON formula %prec EXISTS'''
    p[0] = Node(p[1], [p[2], p[4]])


def p_var_list_many(p):
    'var_list : var_list var_decl'
    p[0] = p[1] + [p[2]]


def p_var_list_one(p):
    'var_list : var_decl'
    p[0] = [p[1]]


def p_var_decl(p):
    'var_decl : name LSQBRACK name RSQBRACK'
    p[0] = Node('VAR', [p[1], p[3]])


def p_formula_atom(p):
    'formula : atom'
    p[0] = p[1]


# Atom
#  (true/false, boolean predicate symbol, predicate symbol with terms, comparison of terms)

def p_atom_bool(p):  # 'true' / 'false'
    '''atom : bool'''
    p[0] = Node('ATOM', [p[1]])


def p_atom_pred_bool(p):  # 'B'
    'atom : name'
    p[0] = Node('ATOM', [PredicateSymbol.node(p[1], arity=0)])


def p_atom_pred(p):  # 'P(T, ...)'
    'atom : name LPAR term_list RPAR'
    (pred, args) = p[1], p[3]
    p[0] = Node('ATOM', [PredicateSymbol.node(pred, arity=len(args))] + args)


def p_atom_cmp(p):  # 't1 = t2' / 't1 =< t2' / ...
    'atom : term comparison term'
    p[0] = Node('ATOM', [Node('CMP', [p[2], p[1], p[3]])])


def p_atom_cmp_quan(p):  # '?=1 x[T] y[T] ...: f' -->  '#{x[T] y[T] ... : f} = 1'
    'atom : EXISTS comparison term var_list COLON formula'
    p[0] = Node(p[1], [p[4], p[6], (p[2], p[3])])
    # p[0] = Node('ATOM', [Node('CMP', [p[2], Node('TERM', [Node('AGGR', ['CNT', p[4], p[6]])]), p[3]])])


def p_atom_cmp_quan_equal(p):  # '?1 x[T]: f'
    'atom : EXISTS term var_list COLON formula'
    p[0] = Node(p[1], [p[3], p[5], (symbol_names['='], p[2])])
    # p[0] = Node('ATOM', [Node('CMP', [symbol_names['='], p[2], Node('TERM', [Node('AGGR', ['CNT', p[3], p[5]])])])])


def p_comparison(p):
    '''comparison : EQ
                  | LEQ
                  | NEQ
                  | GEQ
                  | LT
                  | GT '''
    p[0] = symbol_names[p[1]]


# Term
#  (variable/constant, function symbol with terms, mathematical operation, aggregate function)

def p_term_var(p):  # 'C' / '"a"' / '5'
    'term : name'
    p[0] = Node('TERM', [p[1]])


def p_term_func(p):  # 'F(T, ...)'
    'term : name LPAR term_list RPAR'
    (func, args) = p[1], p[3]
    p[0] = Node('TERM', [FunctionSymbol.node(func, arity=len(args))] + args)


def p_term_par(p):  # '(t)'
    'term : LPAR term RPAR'
    # p[0] = Node('TERM', [Node('PAR', [p[2]])])
    p[0] = p[2]


def p_term_math(p):
    '''term : math
            | abs
            | aggr
            | extremum'''

    p[0] = Node('TERM', [p[1]])


# - Mathematical operations

def p_math_operation(p):  # 't1 + t2' / 't1 - t2' / ...
    '''math : term PLUS term
            | term MINUS term
            | term TIMES term
            | term DIVIDE term
            | term MODULO term'''
    p[2] = symbol_names[p[2]]
    p[0] = Node('MATH', [p[1], p[2], p[3]])


def p_math_operation_prefix(p):  # '+(t1,t2)' / '-(t1,t2)' / ...
    '''math : PLUS LPAR term COMMA term RPAR
            | MINUS LPAR term COMMA term RPAR
            | TIMES LPAR term COMMA term RPAR
            | DIVIDE LPAR term COMMA term RPAR
            | MODULO LPAR term COMMA term RPAR'''
    p[1] = symbol_names[p[1]]
    p[0] = Node('MATH', [p[3], p[1], p[5]])


def p_math_uminus(p):  # '-t'
    'math : MINUS term %prec UMINUS'
    p[0] = Node('MINUS', [p[2]])
    #p[0] = Node('MATH', ['', symbol_names[p[1]], p[2]])


def p_abs(p):  # 'abs(t)'
    'abs : ABS LPAR term RPAR'
    p[0] = Node('ABS', [p[3]])


# - Aggregates

def p_aggr_card(p):  # '#{x[T] y[T] ... : f}' - #, card
    '''aggr : CARD LBRACE var_list COLON formula RBRACE
            | COUNT LBRACE var_list COLON formula RBRACE'''
    p[0] = Node('AGGR', ['CNT', p[3], p[5]])


def p_aggr_func(p):  # 'sum{x[T] ... : f : t}' - sum, max, min, prod
    'aggr : aggregate LBRACE var_list COLON formula COLON term RBRACE'
    p[0] = Node('AGGR', [p[1], p[3], p[5], p[7]])


def p_aggr_list(p):  # 'Sum(t1, t2, ...)' - Sum, Max, Min, Prod
    'aggr : aggregate LPAR term_list RPAR'
    p[0] = Node('AGGRLIST', [p[1]] + p[3])


def p_extremum_min(p):  # 'MIN[:T]'
    'extremum : TYPEMIN LSQBRACK COLON name RSQBRACK'
    #p[0] = _parse_term(f'min{{x[{extract_name(p[4])}] : true : x}}').children[0]
    p[0] = Node('MIN', [p[4]])

def p_extremum_max(p):  # 'MAX[:T]'
    'extremum : TYPEMAX LSQBRACK COLON name RSQBRACK'
    p[0] = Node('MAX', [p[4]])


def p_aggregate_function(p):
    '''aggregate : SUM
                 | PROD
                 | MIN
                 | MAX'''
    p[0] = reserved[p[1]]


def p_term_list_many(p):
    'term_list : term_list COMMA term'
    p[0] = p[1] + [p[3]]


def p_term_list_one(p):
    'term_list : term'
    p[0] = [p[1]]


# Definitions

def p_definition(p):
    'definition : LBRACE definition_body RBRACE'
    p[0] = Node('DEFINITION', p[2])


def p_def_body_many(p):
    'definition_body : definition_body def_rule'
    p[0] = p[1] + [p[2]]


def p_def_body_one(p):
    'definition_body : def_rule'
    p[0] = [p[1]]


def p_def_rule(p):
    'def_rule : def_constr'
    p[0] = Node('DEFIMP', p[1])


def p_def_quan(p):  # '!x[T]: !y[T]: ...'
    'def_constr : FORALL var_list COLON def_constr %prec FORALL'
    p[4][0].extend(p[2])
    p[0] = p[4]


def p_def_constr(p):  # 'A <- f.'
    'def_constr : atom DEFIMPL formula DOT'
    p[0] = [[], p[1], p[3]]


def p_def_constr_omit_body(p):  # 'A <- .'
    'def_constr : atom DEFIMPL DOT'
    p[0] = [[], p[1], Node('ATOM', [Node.Bool(True)])]


def p_def_constr_omit_arrow(p):  # 'A.'
    'def_constr : atom DOT'
    p[0] = [[], p[1], Node('ATOM', [Node.Bool(True)])]


# GENERIC ELEMENTS

# Tuple list

def p_tuple_list_many_range(p):  # 'a; b, c; (d, e); f..i; ...'
    'tuple_list : tuple_list SEMICOLON range'
    p[0] = p[1] + p[3]


def p_tuple_list_one_range(p):
    'tuple_list : range'
    p[0] = p[1]


def p_range(p):  # '1..N' -> '1,2,...,N' / 'a..x' -> 'a,b,...,x'
    'range : name DOT DOT name'
    try:
        l = list(range(int(extract_name(p[1])), int(extract_name(p[4])) + 1))
    except ValueError:
        try:
            x = [p[1], p[4]]
            for i in range(len(x)):
                x[i] = extract_name(x[i])
                if x[i].startswith('\'') and x[i].endswith('\''):
                    x[i] = x[i].replace('\'', '"')
                if x[i].startswith('"') and x[i].endswith('"'):
                    x[i] = x[i].strip('"')
            l = [chr(x) for x in range(ord(x[0]), ord(x[1]) + 1)]
        except ValueError:
            raise
        except TypeError:
            raise TypeError(f'Cannot generate range for given interval {x}.')
    p[0] = [Node('TUPLE', [Node('NAME', [x])]) for x in l]


def p_tuple_list_many(p):
    'tuple_list : tuple_list SEMICOLON tuple'
    p[0] = p[1] + [p[3]]


def p_tuple_list_one(p):
    'tuple_list : tuple'
    p[0] = [p[1]]


def p_tuple_par(p):
    'tuple : LPAR element_list RPAR'
    p[0] = Node('TUPLE', p[2])


def p_tuple_nopar(p):
    'tuple : element_list'
    p[0] = Node('TUPLE', p[1])


# Element list
def p_element_list_many(p):  # 'a, b, ...'
    'element_list : element_list COMMA name'
    p[0] = p[1] + [p[3]]


def p_element_list_one(p):
    'element_list : name'
    p[0] = [p[1]]


# Bool

def p_bool(p):  # 'true' / 'false'
    '''bool : FALSE
            | TRUE '''
    p[0] = Node.Bool(p[1])  # = Node('BOOL', [p[1]])


# Name

# todo: create new kind of nodes "object" and "string" to disambiguate between them
def p_name(p):  # 'a' / 'B' / ''c'' / '"d"' / '1'
    '''name : QUOTE name QUOTE'''
            #| APOST name APOST'''
    p[2].set_name(f'"{p[2].get_name()}"')
    p[0] = p[2]


def p_id(p):
    'name : ID'
    if p[1].startswith('\'') and p[1].endswith('\''):
        p[1] = p[1].strip('\'')
        #p[1].set_name(f'"{p[1].get_name()}"')
    p[0] = Node.Name(p[1])


def p_error(s):
    raise YaccError(s)


lexer = lex.lex(debug=False)
parser = yacc.yacc(debug=False, write_tables=False)


# PARSER

def parse(s):
    return parser.parse(s, lexer=lexer)


# Fragment parsers
def _parse_block(s):
    return yacc.yacc(start='idp_block_list').parse(s, lexer=lexer)[0]


def _parse_constraint(s):
    # fw todo: find out why this does not work: return yacc.yacc(start='constraint').parse(s, lexer=lex.lex())
    return _parse_block('theory _:V{' + s + '}').get_body().children[0]


def _parse_formula(s):
    return _parse_constraint(f'{s}.').children[0]


def _parse_term(s):
    return _parse_constraint(f'{s}=0.').children[0].children[0].children[1]


############################################################
#   CONVERTOR                                              #
############################################################

function_list = []  # list of original function symbols


#
#   GENERIC / FINDER VISITORS
#

class Visitor(object):
    """ Main visitor class for default handling. """

    def visit(self, node, *args):
        return node.receive(self, *args)

    def visit_noop_rec(self, node, *args): # todo replace by rec2
        return self.visit_noop_rec2(node, *args)
        #return Node(node.symbol, [self.visit(x, *args) for x in node.children])

    def visit_noop_rec2(self, node, *args):
        return Node(node.symbol, [x if (not x or isinstance(x, str)) else self.visit(x, *args) for x in node.children])

    def visit_noop_norec(self, node, *args):
        return Node(node.symbol, node.children)


class VocabularyVisitor(Visitor):
    """ Generic visitor for vocabulary elements. """

    def visit_VOCABULARY(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_VOCBODY(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_DECLARATION(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_FUNC(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_PFUNC(self, node, *args):
        return self.visit_FUNC(node, *args)

    def visit_PRED(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_PRED_ENUM(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_TUPLE(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_BUILTINTYPE(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_TYPING(self, node, *args):
        return self.visit_noop_rec2(node, *args)

    def visit_ARITY(self, node, *args):
        return self.visit_noop_norec(node, *args)

    def visit_NAME(self, node, *args):
        return self.visit_noop_norec(node, *args)


class StructureVisitor(Visitor):
    """ Generic visitor for structure elements. """

    def visit_STRUCTURE(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_STRUCBODY(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_ASSIGNMENT(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_FUNC(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_PFUNC(self, node, *args):
        return self.visit_FUNC(node, *args)

    def visit_PRED(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_FUNC_ENUM(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_PRED_ENUM(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_TUPLE(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_NVALUED(self, node, *args):
        return self.visit_noop_norec(node, *args)

    def visit_BUILTINTYPE(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_TYPING(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_ARITY(self, node, *args):
        return self.visit_noop_norec(node, *args)

    def visit_NAME(self, node, *args):
        return self.visit_noop_norec(node, *args)


class TheoryVisitor(Visitor):
    """ Generic visitor for theory elements. """

    def visit_THEORY(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_THBODY(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_OPTIMIZE(self, node, *args):
        return self.visit_THEORY(node, *args)

    def visit_OPBODY(self, node, *args):
        return self.visit_THBODY(node, *args)

    def visit_CONSTRAINT(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_LIMP(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_RIMP(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_EQUIV(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_NOT(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_AND(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_OR(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_ATOM(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_PRED(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_TERM(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_ABS(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_MIN(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_MAX(self, node, *args):
        return self.visit_MIN(node, *args)

    def visit_FUNC(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_PFUNC(self, node, *args):
        return self.visit_FUNC(node, *args)

    def visit_DEFINITION(self, node, *args):
        return self.visit_noop_rec2(node, *args)

    def visit_DEFIMP(self, node, *args):  # !(var1 var2 ...):(head)<-(body).
        return Node(node.symbol, [list(map(self.visit, node.children[0])),
                                  self.visit(node.children[1], *args), self.visit(node.children[2], *args)])

    def visit_CMP(self, node, *args):
        return self.visit_noop_rec2(node, *args)

    def visit_MATH(self, node, *args):
        return self.visit_noop_rec2(node, *args)

    def visit_MINUS(self, node, *args):
        return self.visit_noop_rec2(node, *args)

    def visit_AGGRLIST(self, node, *args):
        return self.visit_noop_rec2(node, *args)

    def visit_VAR(self, node, *args):
        return self.visit_noop_norec(node, *args)

    def visit_BUILTINTYPE(self, node, *args):
        return self.visit_noop_rec(node, *args)

    def visit_TYPING(self, node, *args):
        return self.visit_noop_norec(node, *args)

    def visit_ARITY(self, node, *args):
        return self.visit_noop_norec(node, *args)

    def visit_BOOL(self, node, *args):
        return self.visit_noop_norec(node, *args)

    def visit_NAME(self, node, *args):
        return self.visit_noop_norec(node, *args)

    def visit_FORALL(self, node, *args):  # !(var1 var2 ...):(formula)
        return Node(node.symbol, [list(map(self.visit, node.children[0])), self.visit(node.children[1], *args)])

    def visit_EXISTS(self, node, *args):  # ?(var1 var2 ...):(formula)
        new_node = self.visit_FORALL(node, *args)
        if len(node.children) > 2:
            logging.error('\'EXIST\' node should not have more than two child node, information might get lost.')
        return new_node

    def visit_AGGR(self, node, *args):  # (aggr){!(var1 var2 ...): (formula)[: (term)]}
        return Node(node.symbol, [node.children[0], list(map(self.visit, node.children[1]))]
                    + list(map(lambda x: self.visit(x, *args), node.children[2:])))


class Finder(VocabularyVisitor, StructureVisitor, TheoryVisitor):
    """ Find particular kinds of elements in node tree. """
    def visit_noop_rec(self, node, *args):
        return self.visit_noop_rec2(node, *args)
        #return list(set([z for y in [self.visit(x, *args) for x in node.children] for z in y]))

    def visit_noop_rec2(self, node, *args):
        return list(set([z for y in map(self.visit, filter(Finder.op, node.children)) for z in y]))

    def visit_noop_norec(self, node, *args):
        return []

    def visit_FORALL(self, node, *args):
        return list(set([y for x in list(map(self.visit, node.children[0])) for y in x]
                        + self.visit(node.children[1], *args)))

    def visit_EXISTS(self, node, *args):
        return self.visit_FORALL(node, *args)

    def visit_AGGR(self, node, *args):
        return self.visit_noop_rec(Node('', node.children[2:], *args))

    def visit_DEFIMP(self, node, *args):
        return list(set([z for y in list(map(self.visit, filter(Finder.op, node.children[0])))
                         + list(map(self.visit, node.children[1:])) for z in y]))

    def visit_MIN(self, node, *args):
        return set()

    def visit_MAX(self, node, *args):
        return self.visit_MIN(node, *args) # todo rm

    @staticmethod
    def op(x):
        if x and not isinstance(x, str):
            return True
        return False


class PredicatesFinder(Finder):
    """ Find all predicate symbols. """

    def visit_PRED(self, node, *args):
        return [PredicateSymbol(node)]


class FunctionsFinder(Finder):
    """ Find all function symbols. """

    def visit_FUNC(self, node, *args):
        return [FunctionSymbol(node)]


class EnumerationFinder(Finder):
    """ Find the enumeration assigned to a given symbol. """

    def __init__(self, symb):
        self.goal = symb

    def visit_noop_rec(self, node, *args):
        res = [y for y in [self.visit(x) for x in node.children] if y != []]
        if not res:
            return
        if len(res) > 1:
            logging.warning(f"Multiple possible enumerations found for '{Symbol(self.goal)}'")
        return res[-1]

    def visit_DECLARATION(self, node, *args):
        if Symbol(node.children[0]).matches(Symbol(self.goal)):
            return self.visit(node.children[1])
        return []

    def visit_ASSIGNMENT(self, node, *args):
        if Symbol(node.children[0]).matches(Symbol(self.goal)):
            return self.visit(node.children[1])
        return []

    def visit_PRED_ENUM(self, node, *args):
        return node.children

    def visit_FUNC_ENUM(self, node, *args):
        return self.visit_PRED_ENUM(node, *args)


class FreeVarFinder(Finder):
    """ Find all free variables in a node tree. """

    def visit_FORALL(self, node, *args):
        own = set([v.children[0] for v in node.children[0]])
        return set(self.visit(node.children[1])) - own

    def visit_EXISTS(self, node, *args):
        return self.visit_FORALL(node)

    def visit_AGGR(self, node, *args):
        own = set([v.children[0] for v in node.children[1]])
        return set().union(*list(map(self.visit, node.children[2:]))) - own

    def visit_ATOM(self, node, *args):
        return set(super(FreeVarFinder, self).visit_ATOM(node))

    def visit_TERM(self, node, *args):
        if node.children[0].symbol == 'NAME':
            name = extract_name(node)
            try:
                int(name)
                return set()
            except ValueError:
                if name[0] == '\"':
                    return list(set())
                return set([node.children[0]])
        return set([y for x in map(self.visit, node.children) for y in x])

    def visit_DEFIMP(self, node, *args):
        if node.children[0]:
            return self.visit_FORALL(Node('', [node.children[0], node.children[1]]), *args)\
                        .union(self.visit_FORALL(Node('', [node.children[0], node.children[2]]), *args))
        return self.visit(node.children[1]).union(self.visit(node.children[2]))



class VocabMatcher(VocabularyVisitor, TheoryVisitor, StructureVisitor):
    """ Replace all symbols with their matching vocabulary symbol (with typing info). """

    def __init__(self, vocab, freevars = []):
        self.vocab = vocab  # type: Vocabulary
        self.freevars = freevars

    def add_freevars(self, node, *args):
        self.freevars = FreeVarFinder().visit(node)

    def visit_PRED(self, node, *args):
        return self.vocab.get_predicate(node)

    def visit_FUNC(self, node, *args):
        return self.vocab.get_function(node)

    def visit_TERM(self, node, *args):
        if node.children[0] in self.freevars:
            return Node('TERM', [self.vocab.get_function(Node('FUNC', [node.children[0], Node('ARITY', [0])]))])
        return super(VocabMatcher, self).visit_TERM(node, *args)


class CaseMatcher(TheoryVisitor, StructureVisitor, VocabularyVisitor):
    """ Change symbols to (lower) snake_case and variables to upper case. """

    def __init__(self, mapping):
        self.mapping = mapping

    def visit_PRED(self, node, *args):
        try:
            p_mapped = self.mapping[PredicateSymbol(node)]
        except KeyError:
            raise KeyError(f'Predicate {PredicateSymbol(node)} not in list of symbols.')
        return Node(node.symbol, [Node.Name(p_mapped)] + list(map(self.visit, node.children[1:])))

    def visit_FUNC(self, node, *args):
        try:
            f_mapped = self.mapping[FunctionSymbol(node)]
        except KeyError:
            raise KeyError(f'Function {PredicateSymbol(node)} not in list of symbols.')
        return Node(node.symbol, [Node.Name(f_mapped)] + list(map(self.visit, node.children[1:])))

    def visit_TYPING(self, node, *args):
        try:
            t_mapped = list(map(lambda t: self.mapping[PredicateSymbol.typeSymbol(t)], node.children))
        except KeyError:
            raise KeyError('Type(s) ' + ", ".join([f'"{PredicateSymbol.typeSymbol(x)}"' for x in node.children
                            if PredicateSymbol.typeSymbol(x) not in self.mapping.keys()]) + ' not in list of symbols.')
        return Node(node.symbol, list(map(Node.Name, t_mapped)))

    def visit_VAR(self, node, *args):
        return Node('VAR', [Node.Name(extract_name(node).upper()),
                            Node.Name(self.mapping[PredicateSymbol.typeSymbol(node.children[1])])])

    def visit_MIN(self, node, *args):
        return Node('MIN', [Node.Name(self.mapping[PredicateSymbol.typeSymbol(node.children[0])])])

    def visit_MAX(self, node, *args):
        return Node('MAX', [Node.Name(self.mapping[PredicateSymbol.typeSymbol(node.children[0])])])

    def visit_TERM(self, node, *args):
        if node.children[0].symbol == 'NAME':
            term = extract_name(node)
            if term[0] == '"':
                return Node('TERM', [Node.Name(term.lower())])
            return Node('TERM', [Node.Name(term.upper())])
        return super(CaseMatcher, self).visit_TERM(node, *args)


#
#   NORMALISATION VISITORS
#

# vocabulary

class FunctionDeclarationEliminator(VocabularyVisitor, TheoryVisitor):
    """ Change function declarations into predicate declarations. Keep dict of converted functions. """

    def __init__(self):
        self.converted = {}

    def visit_FUNC(self, node, *args):
        function_list.append(node)
        self.converted[node.get_name()] = node
        return Node('PRED', [self.visit(x) for x in node.children])


# structure

class FullyInterpretedPredicatesFinder(PredicatesFinder):
    """ Find all predicate symbols that do not have a three-valued interpretation. """
    def visit_ASSIGNMENT(self, node, *args):
        if node.children[2].children[0] is not None:  # three-valued
            return []
        return self.visit(node.children[0])


class ArityAdder(StructureVisitor):
    """ Add symbols' arity info (based on assigned enumeration)."""

    def visit_ASSIGNMENT(self, node, *args):
        if any([x for x in node.children[0].children if x.symbol == 'ARITY']):  # node already has arity information
            return node
        try:
            n = self.visit(node.children[1])
        except IndexError:
            pass
        else:
            node.children[0].children.append(Node('ARITY', [n]))
        return node

    def visit_PRED_ENUM(self, node, *args):
        try:
            return len(node.children[0].children)
        except IndexError:
            raise IndexError(f"No interpretation found for '{str(node)}'.")

    def visit_FUNC_ENUM(self, node, *args):
        return self.visit_PRED_ENUM(node, *args) - 1


class FunctionAssignmentEliminator(StructureVisitor):
    """ Change all function assignments to predicate assignments. """

    def visit_PRED(self, node, *args):
        return node

    def visit_FUNC(self, node, *args):
        return Node('PRED', [self.visit(x) for x in node.children])

    def visit_PFUNC(self, node, *args):
        return self.visit_FUNC(node)

    def visit_ARITY(self, node, *args):
        node.children[0] += 1
        return node

    def visit_FUNC_ENUM(self, node, *args):
        return Node('PRED_ENUM', node.children)


# theory

class DefinedSymbolFinder(Finder):
    """ Find the symbol being defined in the definition. """
    def visit_DEFINITION(self, node, *args):
        return list({y for x in list(map(self.visit, node.children)) for y in x})

    def visit_DEFIMP(self, node, *args):
        return self.visit(node.children[1])

    def visit_ATOM(self, node, *args):
        return self.visit(node.children[0])

    def visit_TERM(self, node, *args):
        return self.visit(node.children[0])

    def visit_CMP(self, node, *args):
        return self.visit(node.children[1])

    def visit_PRED(self, node, *args):
        return [PredicateSymbol(node)]

    def visit_FUNC(self, node, *args):
        return [FunctionSymbol(node)]


class PredicateReplacer(TheoryVisitor):
    """ Rename all occurrences of a given predicate. """

    def __init__(self, symb, name):
        self.goal = symb
        self.name = name

    def visit_PRED(self, node, *args):
        if PredicateSymbol(node).matches(self.goal):
            return Node('PRED', [Node.Name(self.name)] + node.children[1:])
        return node


class FunctionEliminator(TheoryVisitor):
    """ Remove functions by replacing them with auxiliary variables and adding the function's predicate representation
        to the constraint, mapping the auxiliary variable to the function arguments.
    """

    def __init__(self, vocab=None):
        self.positive = True
        self.counter = 0
        self.vocab = vocab  # needed if typing info of function symbol is missing

    def new_var(self):
        """ return new variable name """
        self.counter += 1
        return Node.Name(f'_x{str(self.counter - 1)}')

    @staticmethod
    def make_pred(eq):
        """ create predicate 'PF(..., x)' from eq = ('x', 'T', 'PF(...)')."""
        return f'{extract_name(eq[2])}({",".join(map(IDPTheory().visit, eq[2].children[1:] + [eq[0]]))})'

    @staticmethod
    def make_var(eq):
        """ create variable 'x[T]' from eq = ('x', 'T', 'PF(...)')."""
        return f'{extract_name(eq[0])}[{extract_name(eq[1])}]'

    def expand_function(self, atom, eqs):
        if eqs == []:  # no function(s) to expand, return atom
            return atom
        vars_ = ' '.join(map(self.make_var, eqs))
        body = " & ".join(map(self.make_pred, eqs))
        if len(eqs) > 1:
            body = f'({body})'
        atom = IDPTheory().visit(atom)
        if self.positive:   # !x: P(F(x)) -> !x: ?y: F(x,y) & P(y).
            node = _parse_formula(f'? {vars_} :  {body} & {atom}')
        else:               # !x: ~P(F(1)) -> !x: !y: F(x,y) => ~P(y).
            node = _parse_formula(f'! {vars_} :  {body} => {atom}')
        return node

    def visit_CONSTRAINT(self, node, *args):
        self.positive = True
        return super(FunctionEliminator, self).visit_CONSTRAINT(node, *args)

    def visit_NOT(self, node, *args):
        self.positive = not self.positive
        return super(FunctionEliminator, self).visit_NOT(node, *args)

    def visit_ATOM(self, node, *args):
        eqs = []
        atom = super(FunctionEliminator, self).visit_ATOM(node, eqs)
        return self.expand_function(atom, eqs)

    def visit_TERM(self, node, *args):
        if node.children[0].symbol in ['FUNC', 'PFUNC']:
            func_ = node.children[0]  # function symbol
            args_ = node.children[1:]  # arguments
            try:  # return type
                type_ = FunctionSymbol(func_).return_type()
            except AttributeError:
                try:
                    type_ = self.vocab.get_return_type(func_)
                except AttributeError:
                    print('Function symbol typing is missing, make sure it is there '
                          'or provide the vocabulary where to find it.')
                    raise
            var = self.new_var()
            args[0].append((var, type_, Node('TERM', [func_] + [self.visit(x, *args) for x in args_])))
            return Node('TERM', [var])
        return super(FunctionEliminator, self).visit_TERM(node, *args)

    def visit_PRED(self, node, *args):
        return node

    def visit_FUNC(self, node, *args):
        return Node('PRED', [self.visit(x) for x in node.children])

    def visit_PFUNC(self, node, *args):
        return self.visit_FUNC(node)

    def visit_ARITY(self, node, *args):
        node.children[0] += 1
        return node

    def visit_AGGR(self, node, *args):  # (aggr){!(var1 var2 ...): (formula)[: (term)]}
        # convert formula
        node.children[2] = self.visit(node.children[2])

        # convert term
        try:
            term = node.children[3]
        except IndexError:  # no term
            return node
        if term.children[0].symbol == 'NAME':  # simple term
            return node
        eqs = []
        node.children[3] = self.visit(node.children[3], eqs)

        # update formula
        phi = list(map(lambda x: Node('ATOM', list(map(self.visit, x[2].children)) + [Node('TERM', [x[0]])]), eqs))
        node.children[2] = Node('AND', [self.visit(node.children[2])] + phi)

        # update variable list
        node.children[1] += list(map(lambda x: Node('VAR', [x[0], x[1]]), eqs))
        return node

    def visit_DEFIMP(self, node, *args):
        self.positive = True
        if node.children[1].symbol != "ATOM":
            raise ValueError("Definition head can only contain a single atom, "
                             f"but found '{IDPTheory().visit(node.children[1])}' instead.")

        eqs = []  # eq = (var, type, func), e.g., ('xn+1','Tn+1', 'f(x0,...,xn)')
        atom = super(FunctionEliminator, self).visit_ATOM(node.children[1], eqs)
        if eqs == []:
            node.children[2] = self.visit(node.children[2])
            return node

        symb = DefinedSymbolFinder().visit(node)[0].to_node()
        if atom.children[0] == symb:
            head = atom
            body = " & ".join(map(self.make_pred, eqs))
        else:
            i = [i for i, x in enumerate(eqs) if symb == x[2].children[0]][0]
            head = Node('ATOM', list(map(self.visit, eqs[i][2].children)) + [Node('TERM', [eqs[i][0]])])
            body = " & ".join(list(map(self.make_pred, eqs[:i]+eqs[i+1:])) + [IDPTheory().visit(atom)])
        body = Node('AND', [self.visit(_parse_formula(body)), self.visit(node.children[2])])
        vars_ = node.children[0] + list(map(lambda x: Node('VAR', [x[0], x[1]]), eqs))
        return Node('DEFIMP', [vars_, head, body])

    def visit_OPBODY(self, node, *args):
        eqs = []
        term = self.visit(node.children[0], eqs)
        term = Theory.constraint_to_idp(term).strip('(').strip(')')
        if eqs:
            term = f'sum{{{self.make_var(eqs[0])}: {self.make_pred(eqs[0])} : {term}}}'
            return Node('OPBODY', [_parse_term(term)])
        return node


class ExistsEliminator(TheoryVisitor):
    """ Prepare quantors for further conversion.
        e.g., `?x[T]: P(x).`    => `#{x[T]: P(x)} ~= 0 `
        e.g., `?*i x[T]: P(x).` => `#{x[T]: P(x)} * i` with `*` comparison operator and `i` an integer value
    """

    def visit_NOT(self, node, *args):
        is_neg = not args[0] if args else True
        child = self.visit(node.children[0], is_neg)
        try:  # Came across a universal quantification?
            (child, is_hidden_exists) = child
        except TypeError:
            pass
        else:
            if child.symbol != 'FORALL':
                raise ValueError(f'Got an unexpected {child.symbol} node where a "FORALL" child node was expected.')

            if is_hidden_exists:
                return self.visit_EXISTS(child)
        return Node('NOT', [child])

    def visit_FORALL(self, node, *args):
        if args:
            return super(ExistsEliminator, self).visit_FORALL(node), args[0]
        return super(ExistsEliminator, self).visit_FORALL(node)

    def visit_EXISTS(self, node, *args):
        aggr_term = Node('TERM', [Node('AGGR', ['CNT', node.children[0], self.visit(node.children[1])])])
        if len(node.children) > 2:
            aggr_cmp, aggr_val = node.children[2][:2]
            return Node('ATOM', [Node('CMP', [aggr_cmp, aggr_term, aggr_val])])  # ?*i x:f. -> #{x:f}*i.
        return Node('ATOM', [Node('CMP', ['NEQ', aggr_term, Node('TERM', [Node.Name('0')])])])  # ?x:f.    -> #{x:f}~=0.


class QuantorSplitter(TheoryVisitor):
    """ Split quantor over variables into a seperate quantor for each variable.
        e.g., `!x1[T1] x2[T2] ... :` => `!x1[T1]: !x2[T2]: ...`
    """

    def visit_FORALL(self, node, *args):
        vars = node.children[0]
        if len(vars) == 1:
            return super(QuantorSplitter, self).visit_FORALL(node, *args)
        return Node(node.symbol, [[vars[0]], self.visit(Node(node.symbol, [vars[1:], node.children[1]]))])


class ImplicationExpander(TheoryVisitor):
    """ Expand implications to their (simple) logical form.
        e.g., `A <=> B` -> `A => B & A <= B` -> `(~A | B) & (A | ~B)`
    """

    def visit_LIMP(self, node):
        return Node('OR', [Node('NOT', [self.visit(node.children[0])]), self.visit(node.children[1])])

    def visit_RIMP(self, node):
        return Node('OR', [Node('NOT', [self.visit(node.children[1])]), self.visit(node.children[0])])

    def visit_EQUIV(self, node):
        return Node('AND', [self.visit_LIMP(node), self.visit_RIMP(node)])


class Negator(TheoryVisitor):
    """ Negate/invert atoms or terms.
        e.g., `f.` <=> `not f.`
        e.g., `t` <=> `-t`
    """

    def visit_CONSTRAINT(self, node, *args):
        child = node.children[0]
        if child.symbol == "NOT":
            return Node('CONSTRAINT', [self.visit(child.children[0], *args)])
        return Node('CONSTRAINT', [Node('NOT', [self.visit(node.children[0], *args)])])

    def visit_TERM(self, node, *args):
        if node.children[0].symbol == "MINUS":
            return node.children[0].children[0]
        return Node('TERM', [Node('MINUS', [node])])


class AggregateNormalizer(TheoryVisitor): 
    """ normalize aggregates
        e.g., `sum{x[T]: phi: t(T)}.` => `sum{x[T] y[T]: phi & t(T)=y: y}`
    """
    def __init__(self):
        self.counter = 0

    def new_var(self):
        self.counter += 1
        return Node.Name(f'_z{str(self.counter - 1)}')

    def visit_AGGR(self, node, *args):
        try: # no term to normalize, i.e., aggregate count (#{x[T]:phi})
            t = node.children[3]
        except IndexError:
            return node

        if t.children[0].symbol == 'NAME':  # term already normalized
            return node

        if t.children[0].symbol == 'MATH':
            raise NotImplementedError('Cannot convert aggregates with a mathematical aggregate term because of domain.')
        try:
            type_ = FunctionSymbol(t.children[0]).return_type()
        except ValueError:
            raise
        var = self.new_var()
        node.children[1].append(Node('VAR', [var, type_]))
        phi = Node('AND', [self.visit(node.children[2]), Node('ATOM', [Node('CMP', ['EQ', t, Node('TERM', [var])])])])
        return Node(node.symbol, [node.children[0], node.children[1], phi, Node('TERM', [var])])


class AggregateCountToSum(TheoryVisitor):
    """ convert cardinality to sum
        e.g., `#{x[T]:phi}` => `sum{x[T]: phi: 1}`
    """
    def visit_AGGR(self, node, *args):
        if node.children[0] == 'CNT':
            node.children[0] = 'SUM'
            node.children.append(Node('TERM', [Node.Name(1)]))
        return node


class NNF(TheoryVisitor):
    """ Convert negations to Negation Normal Form (=negation only applied directly to atoms)
        e.g., `~(A|(B&~C))` -> `~A & (~B|C)`
    """

    @staticmethod
    def flip(symbol):
        # flipped = {'=': '~=', '<': '>=', '>': '=<', 'OR': 'AND', 'FORALL': 'EXISTS'}
        flipped = {'EQ': 'NEQ', 'LT': 'GEQ', 'GT': 'LEQ', 'OR': 'AND', 'FORALL': 'EXISTS', 'TRUE': 'FALSE'}
        flipped.update({v: k for (k, v) in list(flipped.items())})
        return flipped[symbol]

    def visit_NOT(self, node):
        child = node.children[0]
        if child.symbol == "ATOM":
            grchild = child.children[0]
            if grchild.symbol == 'CMP':  # ~(x>y)  -> x=<y
                return Node('ATOM', [Node('CMP', [NNF.flip(grchild.children[0])] + grchild.children[1:])])
            if grchild.symbol == 'BOOL':  # ~true -> false  /  ~false -> true
                return Node.Bool(NNF.flip(grchild.children[0]))
            return node
        if child.symbol == 'NOT':  # ~~A -> A
            return self.visit(child.children[0])
        if child.symbol in ['AND', 'OR']:  # ~(A&B)  -> ~A|~B
            return Node(NNF.flip(child.symbol), [self.visit(Node('NOT', [x])) for x in child.children])
        if child.symbol in ['FORALL', 'EXISTS']:  # ~!x: A. -> ?x: ~A.
            return Node(NNF.flip(child.symbol), [child.children[0], self.visit(Node('NOT', [child.children[1]]))])


#
#   IDP CONVERSION VISITORS
#

idp_symbol = {'EQ': '=', 'NEQ': '~=', 'LT': '<', 'GT': '>', 'LEQ': '=<', 'GEQ': '>=',
              'CNT': '#', 'SUM': 'sum', 'MIN': 'min', 'MAX': 'max', 'PROD': 'prod',
              'ABS': 'abs', 'PLUS': '+', 'MINUS': '-', 'TIMES': '*', 'DIVIDE': '/', 'MODULO': '%',
              'TRUE': 'true', 'FALSE': 'false'}


class IDPVocabulary(VocabularyVisitor):
    """ Create IDP vocabulary from Vocabulary node. """

    def visit_VOCABULARY(self, node):
        return (f'vocabulary {self.visit(node.children[0])}' +
                '{\n' + self.visit(node.children[2]) + '\n}')

    def visit_VOCBODY(self, node):
        return '\t' + '\n\t'.join(list(map(self.visit, node.children)))

    def visit_DECLARATION(self, node, *args):
        symb = self.visit(node.children[0])
        if not isinstance(symb, list):
            symb = [symb]
        try:
            symb.insert(1, self.visit(node.children[1]))
        except IndexError:
            pass
        return ' '.join(symb)

    def visit_PRED(self, node):
        if node.children[1].symbol == 'ARITY':
            if len(node.children) > 2 and node.children[2].symbol == 'BUILTINTYPE':
                return [f'type {self.visit(node.children[0])}', self.visit(node.children[2])]
            return f'type {self.visit(node.children[0])}'
        args = self.visit(node.children[1])
        if len(args) == 0:
            return self.visit(node.children[0])
        return f'{self.visit(node.children[0])}({",".join(args)})'

    def visit_FUNC(self, node):
        args = self.visit(node.children[1])
        if len(args) == 1:
            return f'{self.visit(node.children[0])}: {args[0]}'
        return f'{self.visit(node.children[0])}({",".join(args[:-1])}): {args[-1]}'

    def visit_PFUNC(self, node):
        return f'partial {self.visit_FUNC(node)}'

    def visit_PRED_ENUM(self, node, *args):
        if node.children[0].symbol == 'TUPLE':
                return f'= {{ {"; ".join(list(map(self.visit, node.children)))} }}'
        return f'constructed from {{ {", ".join(list(map(self.visit, node.children)))} }}'

    def visit_TYPING(self, node):
        return [self.visit(x) for x in node.children]

    def visit_TUPLE(self, node):
        return self.visit(node.children[0])

    def visit_TERM(self, node):
        return self.visit(node.children[0])

    def visit_NAME(self, node):
        return extract_name(node)

    def visit_BUILTINTYPE(self, node, *args):
        return f'isa {self.visit(node.children[0])}'


class IDPStructure(StructureVisitor):
    """ Create IDP structure from structure node. """

    def visit_STRUCTURE(self, node, *args):
        return (f'structure {self.visit(node.children[0])}: {self.visit(node.children[1])}' +
                '{\n' + self.visit(node.children[2]) + '\n}')

    def visit_STRUCBODY(self, node, *args):
        return '\t' + '\n\t'.join(list(map(self.visit, node.children)))

    def visit_ASSIGNMENT(self, node, *args):
        try:
            arity = Symbol(node).arity
        except AttributeError:
            arity = -1
        return f'{self.visit(node.children[0])}{self.visit(node.children[2])} = {self.visit(node.children[1], arity)}'

    def visit_PRED(self, node, *args):
        return self.visit(node.children[0])

    def visit_PRED_ENUM(self, node, *args):
        if len(node.children) == 0 and args[0] == 0:  # boolean 'false' (='{}')
            return 'false'
        elif len(node.children) == 1 and len(node.children[0].children) == 0:  # boolean 'true' (='{()}')
            return 'true'
        return '{' + '; '.join(list(map(self.visit, node.children))) + '}'

    def visit_FUNC(self, node, *args):
        return self.visit(node.children[0])

    def visit_FUNC_ENUM(self, node, *args):
        if len(node.children) == 1:
            if len(node.children[0].children) == 1:
                return self.visit(node.children[0])
        return '{' + '; '.join(list(map(lambda x: self.visit(x)[::-1].replace(',', '>-', 1)[::-1], node.children))) + '}'

    def visit_NVALUED(self, node, *args):
        if node.children[0] is True:
            return '<ct>'
        elif node.children[0] is False:
            return '<cf>'
        return ''

    def visit_TUPLE(self, node, *args):
        return ','.join(list(map(self.visit, node.children)))

    def visit_BOOL(self, node):
        return idp_symbol[node.children[0]]

    def visit_NAME(self, node):
        return extract_name(node)


class IDPTheory(TheoryVisitor):
    """ Create IDP theory from Theory node. """

    def visit_THEORY(self, node, *args):
        return (f'theory {self.visit(node.children[0])}: {self.visit(node.children[1])}' +
                '{\n' + self.visit(node.children[2]) + '\n}')

    def visit_OPTIMIZE(self, node, *args):
        return (f'term {self.visit(node.children[0])}: {self.visit(node.children[1])}' +
                '{\n\t' + self.visit(node.children[2]) + '\n}')

    def visit_THBODY(self, node, *args):
        return '\t' + '\n\t'.join(list(map(self.visit, node.children)))

    def visit_CONSTRAINT(self, node, *args):
        return f'{self.visit(node.children[0])}.'

    # Formula's
    def visit_LIMP(self, node, *args):
        return ' => '.join(list(map(lambda x: f'({self.visit(x)})', node.children)))

    def visit_RIMP(self, node, *args):
        return ' <= '.join(list(map(lambda x: f'({self.visit(x)})', node.children)))

    def visit_EQUIV(self, node, *args):
        return ' <=> '.join(list(map(lambda x: f'({self.visit(x)})', node.children)))

    def visit_NOT(self, node, *args):
        return f'~({self.visit(node.children[0])})'

    def visit_AND(self, node, *args):
        return ' & '.join(list(map(lambda x: f'({self.visit(x)})', node.children)))

    def visit_OR(self, node, *args):
        return ' | '.join(list(map(lambda x: f'({self.visit(x)})', node.children)))

    def visit_FORALL(self, node, *args):
        return f'!{" ".join(list(map(self.visit, node.children[0])))}: {self.visit(node.children[1])}'

    def visit_EXISTS(self, node, *args):
        if len(node.children) > 2:
            return (f'?{idp_symbol[node.children[2][0]]}{self.visit(node.children[2][1])} ' +
                    f'{" ".join(list(map(self.visit, node.children[0])))}: {self.visit(node.children[1])}')
        return f'?{" ".join(list(map(self.visit, node.children[0])))}: {self.visit(node.children[1])}'

    # Atoms
    def visit_ATOM(self, node, *args):
        args = '(' + ','.join([self.visit(x) for x in node.children[1:]]) + ')'
        if args == '()':  # Don't bother printing empty parentheses
            args = ''
        return self.visit(node.children[0]) + args

    def visit_PRED(self, node, *args):
        return self.visit(node.children[0])

    def visit_CMP(self, node, *args):
        t = [self.visit(x) for x in node.children[1:]]
        for i in range(len(t)):
            try:
                if node.children[i + 1].children[0].symbol == 'MATH':
                    t[i] = t[i][1:-1]
            except AttributeError:
                pass

        return (f' {idp_symbol[node.children[0]]} ').join(t)

    def visit_BOOL(self, node, *args):
        return idp_symbol[extract_name(node)]

    # Terms
    def visit_TERM(self, node, *args):
        if len(node.children) > 1:
            return f'{self.visit(node.children[0])}({",".join(list(map(self.visit, node.children[1:])))})'
        return self.visit(node.children[0])

    def visit_ABS(self, node, *args):
        if node.children[0].children[0].symbol == 'MATH':
            return f'abs{self.visit(node.children[0])}'
        return f'abs({self.visit(node.children[0])})'

    def visit_MIN(self, node, *args):
        return f'MIN[:{self.visit(node.children[0])}]'

    def visit_MAX(self, node, *args):
        return f'MAX[:{self.visit(node.children[0])}]'

    def visit_MATH(self, node, *args):
        if len(node.children) > 3:
            raise NotImplementedError('Chained mathematical operations not (yet) implemented.')
        if not node.children[0]:  # UMINUS 
            return f'({idp_symbol[node.children[1]]}{self.visit(node.children[2])})'
        return f'({self.visit(node.children[0])}{idp_symbol[node.children[1]]}{self.visit(node.children[2])})'

    def visit_MINUS(self, node, *args):
        return f'({idp_symbol[node.symbol]}{self.visit(node.children[0])})'

    def visit_AGGR(self, node, *args):
        return (idp_symbol[node.children[0]] + '{' + " ".join(list(map(self.visit, node.children[1]))) + ' : ' +
                " : ".join(list(map(self.visit, node.children[2:]))) + '}')

    def visit_AGGRLIST(self, node, *args):
        return f'{idp_symbol[node.children[0]]}({", ".join(list(map(self.visit, node.children[1:])))})'

    def visit_FUNC(self, node, *args):
        return self.visit_PRED(node, *args)

    # Definitions
    def visit_DEFINITION(self, node, *args):
        return '{\n\t\t' + '\n\t\t'.join(list(map(self.visit, node.children))) + '\n\t}'

    def visit_DEFIMP(self, node, *args):
        if node.children[0]:
            return f'{self.visit(Node("FORALL", node.children[0:2]))} <- {self.visit(node.children[2])}.'
        return f'{self.visit(node.children[1])} <- {self.visit(node.children[2])}.'

    # (General)
    def visit_NAME(self, node, *args):
        return node.children[0]

    def visit_VAR(self, node, *args):
        return f'{self.visit(node.children[0])}[{self.visit(node.children[1])}]'


#
#   ASP CONVERSION VISITORS
#

asp_symbol = {'EQ': '=', 'NEQ': '!=', 'LT': '<', 'GT': '>', 'LEQ': '<=', 'GEQ': '>=',
              'CNT': '#count', 'SUM': '#sum', 'MIN': '#min', 'MAX': '#max',
              'PLUS': '+', 'MINUS': '-', 'TIMES': '*', 'DIVIDE': '/', 'MODULO': '\\'}


class ASPGenerateDefine(StructureVisitor, VocabularyVisitor):
    """ Create ASP define and generate part from structure (and vocabulary) assignments and vocabulary declarations"""

    def __init__(self, struc=None):
        self.assigned = struc.get_fully_interpreted_predicates() if struc else []

    def visit_VOCABULARY(self, node, *args):
        return self.visit(node.children[2])

    def visit_VOCBODY(self, node, *args):
        return '\n'.join(list(filter(None, [self.visit(x) for x in node.children])))

    def visit_DECLARATION(self, node, *args):
        symb = PredicateSymbol(node.children[0])
        try:  # (assignment in structure)
            symb.find_in(self.assigned)
            pass
        except ValueError:
            if len(node.children) > 1:  # assignment
                return self.visit(Node("ASSIGNMENT", node.children + [Node('NVALUED', [None])]))
            else:  # declaration
                if not hasattr(symb, 'typing'):
                    raise AttributeError('Symbol typing missing for generating ASP choice rule for '
                                         f'"{extract_name(node)}". Types or unsorted predicates should '
                                         'be interpreted by the structure or assigned values in vocabulary.')
                if symb.typing:
                    args, vars = zip(*self.visit(node.children[0].children[1]))
                    return f'{{ {self.visit(node.children[0])}({", ".join(vars)}) }} :- {", ".join(args)}.'
                return f'{{ {self.visit(node.children[0])} }}.'
        return

    def visit_STRUCTURE(self, node, *args):
        return self.visit(node.children[2])

    def visit_STRUCBODY(self, node, *args):
        return '\n'.join(filter(None, map(self.visit, node.children)))

    def visit_ASSIGNMENT(self, node, *args):
        is_false = (node.children[2].children[0] is False)
        if PredicateSymbol(node.children[0]).arity == 0:
            if len(node.children[1].children) == 0:  # boolean false
                return f'not not {self.visit(node.children[0])}.' if is_false else ''
            if len(node.children[1].children[0].children) == 0:  # boolean true
                return ('not ' if is_false else '') + f'{self.visit(node.children[0])}.'
        return ' '.join(list(map(lambda x: ('not ' if is_false else '') + f'{self.visit(node.children[0])}({x}).',
                                 self.visit(node.children[1]))))

    def visit_PRED(self, node, *args):
        return self.visit(node.children[0])

    def visit_TYPING(self, node, *args):
        return [(f'{self.visit(x)}(X{i})', f'X{i}') for i, x in enumerate(node.children)]

    def visit_PRED_ENUM(self, node, *args):
        return list(map(self.visit, node.children))

    def visit_TUPLE(self, node, *args):
        return ','.join(list(map(self.visit, node.children)))

    def visit_NAME(self, node):
        x = extract_name(node)
        try:
            int(x)
        except ValueError:
            #x = re.sub(r'"', '', x).lower()
            pass
        return x

    def visit_FUNC(self, node, *args):
        raise NotImplementedError('FO function symbols are not supported in ASP.' +
                                  '(There are methods provided for converting them to predicate symbols)')

    def visit_FUNC_ENUM(self, node, *args):
        raise NotImplementedError('FO function assignments are not supported in ASP.')


class ASPTest(TheoryVisitor):
    def __init__(self, vocab=None, struc=None):
        self.rules = []
        self.minimize = ''
        self.param_counter = 0  # counter for auxiliary predicates
        self.var_counter = 0  # counter for auxiliary variables
        self.vocab = vocab  # type: Vocabulary
        self.struc = struc  # type: Structure

    def __str__(self):
        return '\n'.join([f'{x[0]} :- {x[1]}.'.lstrip() for x in self.rules] + [self.minimize]).rstrip() + '\n'

    def new_param(self, var):
        self.param_counter += 1
        if var == [] or var == set([]):
            return f"'p{str(self.param_counter - 1)}"
        else:
            return f"'p{str(self.param_counter - 1)}({', '.join([self.visit(x) for x in var])})"

    def new_var(self):
        self.var_counter += 1
        return f'_Y{str(self.var_counter - 1)}'

    def emit(self, head, body='', var=None, typing=None, eqs=None):
        """ Add ASP rule to list (add any remaining variables' typing to rule first). """
        if eqs:
            body += ASPTest.join_with_commas(eqs, leading_comma=True)
        if var:
            body += ASPTest.make_types(var, typing)
        self.rules.append((head, body))

    @staticmethod
    def freevars(node):
        return FreeVarFinder().visit(node)

    @staticmethod
    def join_with_commas(lst, leading_comma=False):

        if leading_comma:
            return f", {', '.join(lst)}".rstrip(', ')
        return ', '.join(lst)

    @staticmethod
    def make_types(var, typing):
        """ return typed variables to append to formula.
            e.g., x[T] y[T2]: ... -> :- ..., T(x), T2(y).
        """
        res = ', '.join(list(map(lambda x: f'{typing[extract_name(x)]}({extract_name(x)})', var)))
        if res == '':
            return res
        return f', {res}'

    @staticmethod
    def varlist_to_dict(varlist):
        """ Return list of variables (with their typing) as a dict. """
        return dict([(extract_name(x.children[0]), extract_name(x.children[1])) for x in varlist])

    def visit_THEORY(self, node, *args):
        self.visit(node.children[2])
        return str(self)

    def visit_THBODY(self, node, *args):
        list(map(self.visit, node.children))

    def visit_CONSTRAINT(self, node, typing=None):
        self.var_counter = 0
        typing = {}
        self.emit('', f'not {self.visit(node.children[0], typing)}', ASPTest.freevars(node.children[0]), typing)

    # term optimization

    def visit_OPTIMIZE(self, node, *args):
        self.minimize = self.visit(node.children[2], *args)
        return str(self)

    def visit_OPBODY(self, node, minimize=True):
        term = node.children[0].children[0]
        if term.children[0] != 'SUM':
            raise NotImplementedError('Only constant terms or simple aggregates can be optimized yet.')
        varlist = term.children[1]
        formula = self.visit(term.children[2], ASPTest.varlist_to_dict(varlist))
        if self.rules:
            formula = self.rules[-1][1]
            self.rules = self.rules[:-1]
        x = self.visit(term.children[3]).lstrip('(').rstrip(')')
        vars = f'{x} {" ".join(list(map(extract_name, varlist[:-1])))}'
        if minimize:
            return f'#minimize{{{vars}: {formula}}}.'
        return f'#maximize{{{vars}: {formula}}}.'

    # logical expressions

    def visit_NOT(self, node, typing=None):
        vars = ASPTest.freevars(node)
        term = self.new_param(vars)
        self.emit(term, f'not {self.visit(node.children[0], typing)}', vars, typing)
        return term

    def visit_AND(self, node, typing=None):
        vars = ASPTest.freevars(node)
        term = self.new_param(vars)
        self.emit(term, ', '.join(list(map(lambda x: self.visit(x, typing), node.children))), vars, typing)
        return term

    def visit_OR(self, node, typing=None):
        vars = ASPTest.freevars(node)
        term = self.new_param(vars)
        list(map(lambda x: self.emit(term, self.visit(x, typing), vars, typing), node.children))
        return term

    # quantification

    def visit_FORALL(self, node, typing=None):
        typing.update(ASPTest.varlist_to_dict(node.children[0]))
        var = ASPTest.freevars(node)
        term = self.new_param(var)

        type_var = self.make_types(node.children[0][:1], typing)[2:]
        types = self.make_types(node.children[0], typing)
        agg_val = f'{{ {type_var} : {self.visit(node.children[1], typing)}{types} }} = '

        try:
            type_ = PredicateSymbol.typeSymbol(node.children[0][0].children[1]).to_node()
            nb = str(self.struc.get_nb_of_elements(type_))
        except (AttributeError, TypeError):  # no structure provided, type interpretation not found
            nb_var = self.new_var()
            nb = f'{nb_var}, {nb_var} = {{ {type_var} : {type_var} }}'
        agg_val += nb
        self.emit(term, agg_val, var, typing)
        return term

    def visit_EXISTS(self, node, typing=None):
        typing.update(ASPTest.varlist_to_dict(node.children[0]))
        vars_ = ASPTest.freevars(node)
        term = self.new_param(vars_)
        vars_ch = ASPTest.freevars(node.children[1])
        self.emit(term, self.visit(node.children[1], typing), vars_ch, typing)
        return term

    # mathematical expressions

    def visit_AGGR(self, node, typing=None):
        typing.update(ASPTest.varlist_to_dict(node.children[1]))
        vars = ASPTest.freevars(node)
        eqs = []  # list with equations to add to atom

        if node.children[0] == 'CNT':
            agg = f'{{ {self.visit(node.children[2], typing)} : {self.make_types(node.children[1], typing)[2:]} }}'
        else:
            res = self.visit(node.children[3], typing)
            if isinstance(res, tuple):  # (VAR, VAR = ..., [X, Y, ...], {X:T, Y:T2, ...})
                eqs.extend([res[1]] if not isinstance(res[1], list) else res[1])
                vars = vars.union(res[2])
                typing.update(res[3])
                res = res[0]
            set_ = ', '.join([res] + list(set(map(extract_name, node.children[1]))-{res}))
            symb = (asp_symbol[node.children[0]] if node.children[0] != 'CNT' else '')
            agg = f'{symb}{{{set_}: {self.visit(node.children[2], typing)}{self.make_types(node.children[1], typing)} }}'
        var = self.new_var()
        eqs.append(f'{var} = {agg}')
        return var, eqs, vars, typing

    def visit_AGGRLIST(self, node, *args):
        raise NotImplementedError('Translation of ggregate lists not yet implemented.')

    def visit_CMP(self, node, typing=None):
        vars = ASPTest.freevars(node)
        term = self.new_param(vars)

        eqs = []
        l = self.visit(node.children[1], typing)
        r = self.visit(node.children[2], typing)
        if isinstance(l, tuple):  # (VAR, VAR = ..., [X, Y, ...], {X:T, Y:T2, ...})
            eqs.extend(l[1])
            vars.extend(l[2])
            typing.update(l[3])
            l = l[0]
        if isinstance(r, tuple):
            eqs.extend(r[1])
            vars.extend(r[2])
            typing.update(r[3])
            r = r[0]

        cmp = f'{l} {asp_symbol[node.children[0]]} {r}'
        self.emit(term, cmp, vars, typing, eqs)
        return term

    def visit_ABS(self, node, typing=None):
        return f'|{self.visit(node.children[0], typing)}|'

    def visit_MIN(self, node, *args):
        try:
            type_ = PredicateSymbol.typeSymbol(node.children[0]).to_node()

            return str(self.struc.get_min_of_elements(type_))
        except (AttributeError, TypeError):  # no structure provided, type interpretation not found
            logging.warning(f'Did not find interpretation of "{extract_name(node.children[0])}"'
                            'for MIN of type. Alternative will not work in all cases.')
            pass
        var = self.new_var()
        return f'#min{{{var}:{self.visit(node.children[0])}({var})}}'

    def visit_MAX(self, node, typing=None):
        try:
            type_ = PredicateSymbol.typeSymbol(node.children[0]).to_node()
            return str(self.struc.get_max_of_elements(type_))
        except (AttributeError, TypeError):  # no structure provided, type interpretation not found
            logging.warning(f'Did not find interpretation of "{extract_name(node.children[0])}"'
                            'for MAX of type. Alternative will not work in all cases.')
            pass
        var = self.new_var()
        return f'#max{{{var}:{self.visit(node.children[0])}({var})}}'

    def visit_MATH(self, node, *args):
        if not node.children[0]:  # UMINUS 
            return f'({asp_symbol[node.children[1]]}{self.visit(node.children[2])})'
        return f'({self.visit(node.children[0])}{asp_symbol[node.children[1]]}{self.visit(node.children[2])})'

    def visit_MINUS(self, node, *args):
        return f'({asp_symbol[node.symbol]}{self.visit(node.children[0])})'

    def visit_ATOM(self, node, typing=None):
        args = f'({",".join(list(map(lambda x: self.visit(x, typing), node.children[1:])))})'
        if args == '()':  # don't bother printing empty parentheses
            args = ''
        return self.visit(node.children[0], typing) + args

    # base elements

    def visit_BOOL(self, node, *args):
        if node.get_name() == 'TRUE':
            return '#true'
        return '#false'

    def visit_VAR(self, node, *args):
        return f'{self.visit(node.children[1])}({self.visit(node.children[0])})'

    def visit_FUNC(self, node, typing=None):
        raise NotImplementedError('Functions are not supported in ASP.')

    def visit_PRED(self, node, typing=None):
        return self.visit(node.children[0], typing)

    def visit_TERM(self, node, typing=None):
        return self.visit(node.children[0], typing)

    def visit_NAME(self, node, typing=None):
        return re.sub('"', '', node.children[0])
        # return node.children[0]

    # definitions

    def visit_DEFINITION(self, node, *args):
        #asp_ = ASPTest(self.vocab, self.struc)

        # change definition predicate(s) to auxiliary predicate(s) and add equivalence constraint for these predicates
        for pred in DefinedSymbolFinder().visit(node):
            new_name = '_' + pred.name  # "_p"
            node = PredicateReplacer(pred, new_name).visit(node)
            vars = ['X' + str(i) for i in range(pred.arity)]
            if vars:
                typing = {var_type[0]: extract_name(var_type[1]) for var_type in zip(vars, pred.typing)}
                body = f'{pred.name}({",".join(vars)}), not {new_name}({",".join(vars)})'
                self.emit('', body, vars, typing)
                body = f'not {pred.name}({",".join(vars)}), {new_name}({",".join(vars)})'
                self.emit('', body, vars, typing)
            else:
                self.emit('', f'{pred.name}, not {new_name}')
                self.emit('', f'not {pred.name}, {new_name}')

        # definition constraints
        list(map(lambda x: self.visit(x), node.children))

    def visit_DEFIMP(self, node, *args):
        typing = {}
        vars_ = set(list(ASPTest.freevars(node.children[2])))
        if node.children[0]:
            typing.update(ASPTest.varlist_to_dict(node.children[0]))
            vars_.update(list(map(lambda x: x.children[0], node.children[0])))
        head = self.visit(node.children[1], typing)
        body = self.visit(node.children[2], typing)
        self.emit(head, body, vars_, typing)


#
#   CONVERSION
#

class Block(object):
    def __init__(self, node):
        self.name = extract_name(node)
        self.tree = node  # type: Node

    def __str__(self):
        try:
            voc = self.get_vocab_name()
            return f'{self.get_kind()} {self.name}: {voc}' + '\n\t' + '\n\t'.join(map(str, self.get_body().children))
        except AttributeError:
            pass
        return f'{self.get_kind()} {self.name}' + '\n\t' + '\n\t'.join(map(str, self.get_body().children))

    # getters
    def get_kind(self):
        """ Get the kind of block                       ,e.g., `Structure S: V{<assignments>}` -> `STRUCTURE` """
        return self.tree.symbol

    def get_name(self):
        """ Get the name of the block                   ,e.g., `Structure S: V{<assignments>}` -> `S` """
        return extract_name(self.tree.children[0])

    def get_vocab_name(self):
        """ Get the name of the interpreted vocabulary  ,e.g., `Structure S: V{<assignments>}` -> `V`"""
        if self.tree.symbol == 'VOCABULARY':
            raise AttributeError(f"'{self.name}' is already a vocabulary, it doesn't interpret one itself.")
        return extract_name(self.tree.children[1])

    def get_body(self):
        """ Get the body of the block                   ,e.g., `Structure S: V{<assignments>}` -> `<assignments>`"""
        return self.tree.children[2]

    # finders
    def get_predicates(self):
        """ Get a list of all predicate symbols in the structure. """
        return PredicatesFinder().visit(self.tree)

    def get_fully_interpreted_predicates(self):
        """ Get a list of all predicate symbols that are provided with a complete (two-valued) interpretation.
            (no three-valued interpretation). """
        return FullyInterpretedPredicatesFinder().visit(self.tree)

    def get_functions(self):
        """ Get a list of all function symbols in tree. """
        return FunctionsFinder().visit(self.tree)

    def get_symbols(self):
        """ Get a list of all symbols in tree. """
        return self.get_predicates() + self.get_functions()

    # adders
    def set_body(self, nodes):
        self.tree.children[2] = nodes

    def add_to_body(self, nodes):
        """ Add node(s) to the body of the block. """
        if not isinstance(nodes, list):
            nodes = [nodes]
        self.tree.children[2].children.extend(nodes)

    # permutation visitors
    def match_vocabulary(self, vocab_or_matcher, create_new_matcher=True):
        """ Complete symbol data with typing info from vocabulary. """
        matcher = (VocabMatcher(vocab_or_matcher) if create_new_matcher else vocab_or_matcher)
        matcher.add_freevars(self.tree)
        self.tree = matcher.visit(self.tree)
        return matcher

    def match_naming_convention(self, vocab_or_map, create_new_map=True):
        """ Change symbol and variable names to comply with ASP naming conventions. """
        mapping = (vocab_or_map.get_naming_convention_mapping() if create_new_map else vocab_or_map)
        self.tree = CaseMatcher(mapping).visit(self.tree)
        return mapping

    def convert_functions(self, *args):
        raise NotImplementedError()

    # prints
    def print_symbols(self):
        """ Print all symbols in tree. """
        print(f'Symbols in {self.get_kind().lower()} {self.name}: {", ".join(map(str, self.get_symbols()))}')

    def print(self, print_depth=2):
        """ Print parse tree with a given depth. """
        self.tree.print(print_depth)


class Vocabulary(Block):
    def __init__(self, node):
        """ Create Vocabulary object from tree. Additionally store list of function symbols"""
        super(Vocabulary, self).__init__(node)

    # getters
    def get_declarations(self):
        """ Return all declarations in vocabulary. """
        return self.get_body().children

    # finders
    def get_types(self):
        return list(filter(PredicateSymbol.is_type, self.get_declared_symbols()))

    def get_declared_symbols(self):
        """ Return a list of all symbols that are declared in the vocabulary. """
        return self.get_symbols()

    def get_predicate(self, node):
        """ Find a predicate Node in the list of predicate( symbol)s, return matching predicate(s) as Node objects. """
        return PredicateSymbol(node).find_in(self.get_predicates()).to_node()

    def get_function(self, node):
        """ Find a function Node in the list of functions, return matching function(s) as Node objects. """
        return FunctionSymbol(node).find_in(self.get_functions()).to_node()

    def get_return_type(self, func):
        """ Return the return type of a given function. """
        return FunctionSymbol(self.get_function(func)).return_type()

    def get_naming_convention_mapping(self):
        """ Return a mapping for all symbols in the vocabulary to match the naming conventions of ASP. """
        mapping = {}
        for symb in self.get_declared_symbols():  # change all symbol names to asp standards
            new_name = re.sub('([a-z0-9])([A-Z])', r'\1_\2', re.sub('(.)([A-Z][a-z]+)', r'\1_\2', symb.name)).lower()
            mapping[symb] = new_name
        if len(mapping.values()) == len(set(mapping.values())):  # OK if each symbol name unique
            return mapping

        # make types unique
        for typesymb in self.get_types():
            # if not mapping[typesymb].startswith('type_'):
            #     mapping[typesymb] = 'type_' + mapping[typesymb]
            i = 2
            typename = mapping[typesymb]
            while list(mapping.values()).count(mapping[typesymb]) > 1:
                mapping[typesymb] = typename + str(i)
                i += 1

        # make symbols unique
        for (symb, name) in [(s, n) for (s, n) in list(mapping.items()) if (list(mapping.values()).count(n) > 1)]:  #
            try:
                typing = [y for z in [extract_name(x) for x in symb.typing] for x, y in list(mapping.items()) if
                          x.name == z]
            except AttributeError:
                continue
            mapping[symb] = f"{name}'{'_'.join(typing)}"

            i = 2
            while list(mapping.values()).count(mapping[symb]) > 1:
                mapping[symb] = f"{name}{i}'{'_'.join(typing)}"
                i += 1
        return mapping

    # adders
    def add_declarations(self, nodes):
        """ Add a (list of) symbol interpretation(s) to the structure. """
        self.add_to_body(nodes)

    def merge(self, vocab):
        """ Merge structure with another structure 'struc'. """
        self.add_declarations(vocab.get_declarations())

    # permutations
    def convert_functions(self, theor):
        """ Convert function symbols to predicate symbols. Add implied constraints of converted functions to theory. """

        def func_cons(symb):
            symb = FunctionSymbol(symb)
            args_ = list(map(lambda i: f'x{str(i)}', range(len(symb.typing))))
            vars_ = [f'{x}[{extract_name(t)}]' for x, t in zip(args_, symb.typing)]
            data = {'args': ', '.join(args_), 'vars': ' '.join(vars_[:-1]), 'var': vars_[-1], 'symb': symb.name,
                    'pf': ('<' if symb.is_partial() else '')}
            x = ('! %(vars)s:' % data if args_[:-1] else '') + '#{ %(var)s: %(symb)s(%(args)s) } =%(pf)s 1.' % data
            node = _parse_constraint(x)
            return VocabMatcher(self).visit(node)

        convertor = FunctionDeclarationEliminator()
        self.tree = convertor.visit(self.tree)
        for x in list(convertor.converted.values()):
            theor.add_constraints(func_cons(x))
        return

    def make_opens(self, closed): 
        ''' Returns constraint "{P(X0,X1)} :- T1(X0), T1(X2)." for every predicate P(T1,T2) not closed in structure.'''

        def make_open(pred):
            types = pred.typing
            n = len(types)
            varbase = 'X'
            vars_ = []
            atoms = []
            for i in range(n):
                x = varbase + str(i)
                vars_.append(x)
                atoms.append(f'{extract_name(types[i])}({x})')
            return f'{{ {extract_name(pred.name)}({",".join(vars_)})}} :- {", ".join(atoms)}.'

        def todo_pred(p, closed):
            return not any(p.find_in(closed))

        todo = [x for x in self.get_predicates() if todo_pred(x, closed)]
        return '\n'.join(map(make_open, todo))

    # translations
    def to_idp(self):
        """ Return IDP representation of vocabulary. """
        return IDPVocabulary().visit(self.tree)

    def to_asp(self, struc=None):
        """ Return ASP representation of vocabulary. """
        if self.get_functions() != []:
            raise AttributeError(f'Vocabulary {self.name} cannot be converted to ASP, it contains functions!')
        return ASPGenerateDefine(struc).visit(self.tree)


class Structure(Block):
    def __init__(self, node):
        """ Create Structure object from tree containing a list of symbol interpretations. """
        super(Structure, self).__init__(node)
        self.add_arity()

    # getters
    def get_interpretations(self):
        """ Return all interpretations in the structure. """
        return self.get_body().children

    # finders
    def get_interpretation_of(self, symb):
        """ Return the interpretation of a given predicate symbol by its node or plain name. """
        if isinstance(symb, str):
            symb = Node('PRED', [Node('NAME', [symb])])
        return EnumerationFinder(symb).visit(self.tree)

    def get_nb_of_elements(self, symb):
        """ Return the number of elements in the interpretation of a given predicate symbol. """
        return len(self.get_interpretation_of(symb))

    def get_extremal_of_elements(self, symb, minmax):
        """ Return the min or max value in the interpretation of a given predicate symbol. """
        out = None
        try: # first try to convert all to a float and use value corresponding to extremal float
            out = minmax([(float(x),x) for x in map(extract_name, self.get_interpretation_of(symb))],key = lambda tup: tup[0])[1]
        except: # use alphabetical order
            out = minmax(map(extract_name, self.get_interpretation_of(symb)))
        return out

    def get_max_of_elements(self, symb):
        return self.get_extremal_of_elements(symb, max)

    def get_min_of_elements(self, symb):
        return self.get_extremal_of_elements(symb, min)

    # adders
    def add_interpretations(self, nodes):
        """ Add a (list of) symbol interpretation(s) to the structure. """
        self.add_to_body(nodes)

    def merge(self, struc):
        """ Merge structure with another structure 'struc'. """
        self.add_interpretations(struc.get_interpretations())

    # permutations
    def add_arity(self):
        """ Add the symbols' arity to the symbols in the structure. """
        self.tree = ArityAdder().visit(self.tree)

    def add_typing(self, vocab):
        """ Add the symbols' typing info to the symbols in the structure. """
        self.match_vocabulary(vocab)

    def convert_functions(self):
        """ Convert function assignments to predicate assignments. """
        self.tree = FunctionAssignmentEliminator().visit(self.tree)

    # translations
    def to_idp(self):
        """ Return IDP representation of structure. """
        return IDPStructure().visit(self.tree)

    def to_asp(self):
        """ Return ASP representation of structure. """
        return ASPGenerateDefine().visit(self.tree)


class Theory(Block):
    def __init__(self, node):
        super(Theory, self).__init__(node)

    # getters
    def get_constraints(self):
        """ Return all constraints in the theory. """
        return self.get_body().children

    # adders
    def add_constraints(self, nodes):
        """ Add a (list of) constraint(s) to the theory. """
        self.add_to_body(nodes)

    def merge(self, theor):
        self.add_constraints(theor.get_constraints())
        # todo: remove duplicates

    # permutations
    def expand_implications(self):
        """ Convert implications to their basic logical form. """
        self.tree = ImplicationExpander().visit(self.tree)

    def convert_exists(self):
        """ Convert existential quantors to their comparison equivalent. """
        self.tree = ExistsEliminator().visit(self.tree)

    def convert_functions(self, vocab):
        """ Convert functions to auxiliary variables and map these to the corresponding (function) predicates. """
        self.tree = FunctionEliminator(vocab).visit(self.tree)

    def negate_constraints(self):
        """ Convert constraints to their inverted form. """
        self.tree = Negator().visit(self.tree)

    def nnf(self):
        """ Convert constraints to normal negation form. """
        self.tree = NNF().visit(self.tree)

    def split_forall(self):
        """ Convert all universal quantors over multiple variables to seperate quantors per variable. """
        self.tree = QuantorSplitter().visit(self.tree)

    def normalize_aggregates(self):  
        self.tree = AggregateNormalizer().visit(self.tree)

    def normalize(self):
        self.expand_implications()  # convert implications to basic logical form, e.g., A=>B to ~A|B
        self.split_forall()         # convert quantors to single variable quantors, e.g., !x y ... z: to !x: !y: ... !z:
        self.nnf()                  # put in Normal Negation Form
        # self.convert_exists()     # convert existential quantors to aggregates

    # translations
    def to_idp(self):
        """ Return IDP representation of theory. """
        return IDPTheory().visit(self.tree)

    def to_asp(self, vocab, struc=None):
        """ Return ASP representation of theory. """
        if not struc:
            logging.warning('No structure provided, conversion functionality may be limited.')
        return ASPTest(vocab, struc).visit(self.tree)

    @staticmethod
    def constraint_to_idp(node):
        return IDPTheory().visit(node)


class Term(Theory):
    def __init__(self, node):
        """ Create Term object from tree. """
        super(Term, self).__init__(node)
        self.minimize = True

    # permutations
    def negate_term(self):
        """ Convert term to its inverted form. e.g., t <=> -t """
        self.tree = Negator().visit(self.tree)

    def convert_count_to_sum(self):
        """ Convert cardinality aggregate to sum. """
        self.tree = AggregateCountToSum().visit(self.tree)

    def normalize(self):
        self.convert_count_to_sum()

    # translations
    def to_asp(self, vocab):
        """ Return ASP representation of theory. """
        return ASPTest(vocab).visit(self.tree, self.minimize)


############################################################
#          RUN                                             #
############################################################


def apply(func_, list_, *args, **kwargs):
    list(map(lambda x: func_(x, *args, **kwargs), filter(None, list_)))


class Folasp:
    INFERENCE_DEFAULT = "AUTO"

    def __init__(self, inference_task=None):
        self.idp_vocabularies = OrderedDict({'_V': Node.EmptyBlock('vocabulary', '_V')})
        self.idp_structures = OrderedDict({'_S': Node.EmptyBlock('structure', '_S', '_V')})
        self.idp_theories = OrderedDict({'_T': Node.EmptyBlock('theory', '_T', '_V')})
        self.idp_terms = OrderedDict({})

        self.t = self.s = self.v = self.o = None
        self.inference_task = ()
        self.symbol_map = {}
        self.asp = ""

        if inference_task:
            self.set_inference_task(inference_task)

    # getters

    def get_block(self, name):
        if name in self.idp_vocabularies:
            return self.idp_vocabularies[name]
        if name in self.idp_structures:
            return self.idp_structures[name]
        if name in self.idp_theories:
            return self.idp_theories[name]
        if name in self.idp_terms:
            return self.idp_terms[name]
        return None

    # setters

    def set_inference_task(self, task):
        if not task:
            inference = getattr(self, "INFERENCE_DEFAULT")
            logging.info('No inference task specified. '
                         'AUTO search for term block to minimize, else perform model expand.')
        else:
            inference = re.sub('optimal', '', re.sub('model', '', task[0] if isinstance(task, list) else task)
                                                                                                .strip().upper())
        if inference not in ['AUTO', 'EXPAND', 'MINIMIZE']:
            raise ValueError(f'Specified inference task "{inference}" is not supported.'
                             ' Currently supported task are: -auto -(model)expand -minimize')

        if inference == 'MINIMIZE':
            if isinstance(task, list) and len(task) >= 3:
                raise ValueError('Too many arguments were given for setting the inference task. Expected "minimize"'
                                 f' and optionally a single term to optimize but got ({",".join(task)})')
            elif isinstance(task, list) and len(task) >= 2:
                if type(task[1]) != str:
                    raise ValueError(f'Got unexpected {type(task[1])} object "{task[1]}", '
                                     'expected term name of term to optimize.')
                inference = f'{inference}.{task[1]}'
            else:
                logging.info('For the inference task "minimize" a term can be passed, '
                             'if not, lastly parsed term is optimized.')
        self.inference_task = inference

    # actions
    def clear(self):
        self.t = self.s = self.v = self.o = None
        self.inference_task = ()
        self.asp = ""

    def parse_fol(self, idp):
        """ Parse FOL program. """
        self.clear()

        # remove comments and some other syntax features not parsed by the LY parser.
        idp = re.sub(r'(\/\*(\*(?!\/)|[^*])*\*\/)', '', re.sub(r'(\/\/.*)', '', idp))  # remove comments
        idp = re.sub(r'(procedure .*\{([^\{])*\})', '', idp)  # remove procedure block
        idp = re.sub(r'(;[\ \t\n]*\})', '}', idp)  # remove semi-colon at end of interpretation
        idp = idp.strip()
        if not idp:
            return []

        # parse FOL program
        for block in parse(idp):
            if isinstance(block, Term):
                if block in self.idp_terms:
                    raise KeyError(f"Term name {block} already in use")
                self.idp_terms[block.get_name()] = block
            elif isinstance(block, Theory):
                if block.name in self.idp_theories:
                    raise KeyError(f"Theory name {block.name} already in use")
                self.idp_theories[block.get_name()] = block
            elif isinstance(block, Structure):
                if block in self.idp_structures:
                    raise KeyError(f"Structure name {block} already in use")
                self.idp_structures[block.get_name()] = block
            elif isinstance(block, Vocabulary):
                if block in self.idp_vocabularies:
                    raise KeyError(f"Vocabulary name {block} already in use")
                self.idp_vocabularies[block.get_name()] = block

            else:
                logging.error("Unknown idp block element found.")
        return list(self.idp_vocabularies.values()) + list(self.idp_theories.values()) \
               + list(self.idp_structures.values()) + list(self.idp_terms.values())

    def _configure(self, tsvo=None, inference_task=()):
        """ Configure IDP program, i.e, which IDP blocks to consider. If not specified,
            lastly parsed theory and structure block and the vocabulary of the theory or structure is used. """
        if len(self.idp_vocabularies) <= 1:
            logging.error('No vocabularies found. Your IDP program should have a vocabulary, '
                          + 'if your program has one, parse it first.')
            raise KeyError('No vocabulary found')
        t, s, v, o = None, None, None, None      
        if tsvo:
            t, s, v, o = tsvo

        # check or select idp blocks for configuration
        if t:
            if t not in self.idp_theories:
                raise KeyError(f'Theory "{t}" not found.')
        else:
            logging.debug('No theory specified. ')
            t = list(self.idp_theories.keys())[-1]
        if t == list(self.idp_theories.keys())[0]:
            logging.warning('No theories found. If your IDP program has a theory, parse it first.')
        else:
            logging.debug(f'Theory "{t}" used for configuration.')
        self.t = copy.deepcopy(self.idp_theories[t])

        if s:
            if s not in self.idp_structures:
                raise KeyError(f'Structure "{s}" not found.')
        else:
            logging.debug('No structure specified')
            s = [x for x in self.idp_structures.keys() if x != '_'][-1]
        if s == list(self.idp_structures.keys())[0]:
            logging.warning('No structures found. If your IDP program has a structure, parse it first.')
        else:
            logging.debug(f'Structure "{s}" used for configuration.')
        self.s = copy.deepcopy(self.idp_structures[s])

        if v:
            if v not in self.idp_vocabularies:
                raise KeyError(f'Vocabulary "{v}" not found.')
        else:
            i = 'No vocabulary specified, vocabulary '
            v = list(self.idp_vocabularies.keys())[-1]

            v_ = self.idp_theories[t].get_vocab_name()
            if v_ in self.idp_vocabularies and v_ != list(self.idp_vocabularies.keys())[0]:
                v = v_
                logging.debug(i + f'"{v}" of theory "{t}" will be used.')
                if s != list(self.idp_structures.keys())[0] and \
                        t != list(self.idp_theories.keys())[0] and \
                        self.idp_structures[s].get_vocab_name() != v:
                    logging.warning(f'Theory "{t}" and structure "{s}" '
                                    + 'do not share the same vocabulary.')
            else:
                v_ = self.idp_structures[s].get_vocab_name()
                if v_ in self.idp_vocabularies and v_ != list(self.idp_vocabularies.keys())[0]:
                    v = v_
                    logging.debug(i + f'"{v}" of structure "{s}" will be used.')
        self.v = copy.deepcopy(self.idp_vocabularies[v])
        logging.debug(f'Vocabulary "{self.v.name}" used for configuration.')

        # set inference task
        self.set_inference_task(inference_task)
        if self.inference_task == 'AUTO':
            self.inference_task = ('MINIMIZE' if list(self.idp_terms.keys()) != [] else 'EXPAND')
        if self.inference_task.split('.')[0] == 'MINIMIZE':
            if len(list(self.idp_terms.keys())) < 1:
                raise IndexError(f'No terms parsed. Cannot provide minimize inference task.')
            try:
                o = self.inference_task.split('.')[1]
            except IndexError:
                logging.debug('No term specified for minimize inference task, using lastly parsed term block')
                o = list(self.idp_terms.keys())[-1]
            except KeyError:
                raise KeyError(f'Could not find term "{o}" to minimize.')
            finally:
                logging.debug(f'Term "{o}" used for optimization.')
        else:
            logging.debug('Inference task set to model expand.')
        if o:
            self.o = copy.deepcopy(self.idp_terms[o])

        logging.info('Setting inference task to '
                     + ('expand' if self.inference_task == 'EXPAND' else f'minimize term {o}, expanding')
                     + f' structure {s} for theory {t} over vocabulary {v}')
        return self.t, self.s, self.v, self.o

    def _normalize(self, tsvo=None):
        """ Normalize the parse trees of a FOL program. """
        if tsvo:
            self.t, self.s, self.v, self.o = tsvo

        # match symbol occurrences with vocabulary symbols and add typing info
        match = self.v.match_vocabulary(self.v)
        apply(Block.match_vocabulary, [self.s, self.t, self.o], match, create_new_matcher=False)

        # normalize constraints
        self.t.normalize()

        # convert function symbol( occurrence)s to predicate symbol(s)
        global function_list
        function_list = []
        self.s.convert_functions()
        self.t.convert_functions(self.v)  # use vocab to get typing info of function symbols
        self.v.convert_functions(self.t)  # add implied constraints to theory
        if self.o:
            self.o.convert_functions(self.v) 

        # match new symbols
        match = self.v.match_vocabulary(self.v)
        apply(Block.match_vocabulary, [self.s, self.t, self.o], match, create_new_matcher=False)
        # normalize again
        self.t.normalize()
        if self.o:
            self.o.normalize()
        return self.t, self.s, self.v, self.o

    def _convert(self, tsvo=None):
        """ Convert the parse trees of a normalized FOL program to ASP. """
        if tsvo:
            self.t, self.s, self.v, self.o = tsvo

        # change symbol names to match ASP naming conventions
        self.symbol_map = self.v.match_naming_convention(self.v)
        apply(Block.match_naming_convention, [self.s, self.t, self.o], self.symbol_map, create_new_map=False)

        # translate to ASP program
        asp = filter(None, [self.v.to_asp(self.s), self.s.to_asp(), self.t.to_asp(self.v, self.s),
               (self.o.to_asp(self.v) if self.o else None)])
        return '\n\n'.join(map(str.strip, asp))

    def convert_fol_to_asp(self, theory_name=None, struc_name=None, vocab_name=None, term_name=None,
                           inference_task=None):
        """ Convert FOL program to ASP. """
        self._configure(tsvo=(theory_name, struc_name, vocab_name, term_name), inference_task=inference_task)
        self._normalize()
        return self._convert()

    def convert_asp_output_to_fol(self, asp_output, new_struc_name=None, return_program=False):
        """ Convert the ASP output (back) to the IDP structure and return the complete IDP program. """

        def parse_asp_output(asp):
            """ Parse ASP model. """
            preds = dict()  # dict of predicate symbol interpretations
            bools = list()  # list of boolean truths
            for interpretation in asp.strip().split(' '):
                try:
                    symb, values = interpretation.split('(')
                    if symb not in preds.keys():
                        preds[symb] = list()
                    preds[symb].append(values.rstrip('\n').rstrip(')').split(','))
                except ValueError:
                    bools.append(interpretation)
            return preds, bools

        def convert_asp_output(preds, bools_true, new_struc_name=None):
            """ Convert ASP model to an IDP structure. """
            # get (new) structure name
            if not new_struc_name:
                new_struc_name = self.s.name + '_asp'
            if new_struc_name == self.s.name:
                logging.warning('Original structure will be overwritten ' \
                                + 'by a new structure from the converted ASP output.')
            elif new_struc_name in self.idp_structures:
                logging.warning('Another structure of the IDP input program will be overwritten ' \
                                + 'by a new structure from the converted ASP output.')
            logging.debug(f'Newly generated IDP structure named "{new_struc_name}".')

            # remove auxiliary predicates that were introduced during FOL to ASP conversion.
            for x in [x for x in preds.keys() if (x.startswith("'") or x.startswith("_"))]:
                preds.pop(x)

            # convert to idp structure
            bools = [(k.name, v) for (k, v) in self.symbol_map.items() if k.is_bool()]
            b = [k + (" = True" if v in bools_true else " = False") for (k, v) in bools]

            preds = [(k.name, preds[v]) for (k, v) in self.symbol_map.items() if v in preds]
            funcs = [f.get_name() for f in function_list]
            p = [k + " = { " + '; '.join([','.join(x[:-1]) + ('->' if k in funcs else (',' if len(x) > 1 else ''))
                                          + x[-1] for x in v]) + " } " for (k, v) in preds]

            interpret = '\n\t'.join(b + p)
            s = f"Structure {new_struc_name}:{self.v.name} {{\n\t{interpret} \n}}"
            ps = parse(s)[0]
            return ps

        s_old = self.s
        asp_output = asp_output.strip().split('\n')[-1] # do we want last model? For optimalisation yes and otherwise>
        self.s = convert_asp_output(*parse_asp_output(asp_output), new_struc_name)
        self.idp_structures[self.s.name] = self.s
        if return_program:
            return self.str_idp_by_name([self.v.name, s_old.name, self.s.name, self.t.name])  # return original v and t
        return self.str_idp(self.s)

    def output_fol(self):
        return self.str_idp(list(filter(None, [self.v, self.s, self.t, self.o])))

    # representations

    def str_idp(self, blocks=None):
        """ Return IDP representation for the list of IDP blocks. """
        s = []
        if not isinstance(blocks, list):
            blocks = [blocks]
        for block in blocks:
            s.append(block.to_idp())
        return '\n\n'.join(s)

    def str_idp_by_name(self, names):
        """ Return IDP representation for the list of IDP block names. """
        blocks = []
        if not isinstance(names, list):
            names = [names]
        for name in names:
            blocks.append(self.get_block(name))
        return self.str_idp(blocks)

    def str_tree(self, blocks, print_depth=2):
        """ Print parsed block in tree representation with a given depth. """
        def recurse_print(node, depth=0, indent=0):
            t = ''
            try:
                if depth == indent:
                    return '\n' + '\t' * indent + str(node)
                if node.symbol in ['NAME', 'ARITY', 'TYPING']:
                    return '\n' + '\t' * indent + str(node)  
                t += '\n' + '\t' * indent + str(node.symbol) + '['
                for x in node.children:
                    if not isinstance(x, list):
                        x = [x]
                    for y in x:
                        t += recurse_print(y, depth=depth, indent=indent + 1)
                t += '\n' + '\t' * indent + ']'
            except AttributeError:
                t += '\n' + '\t' * indent + str(node)
                pass
            return t
        s = []
        if not isinstance(blocks, list):
            blocks = [blocks]
        for block in blocks:
            s.append(recurse_print(block.tree, print_depth))
        return '\n'.join(s)

    def str_tree_by_name(self, names, print_depth=2):
        """ Return tree representation for the list of IDP block names. """
        blocks = []
        if not isinstance(names, list):
            names = [names]
        for name in names:
            blocks.append(self.get_block(name))
        return self.str_tree(blocks, print_depth)


class ClingoUnsatError(Exception):
    pass


class ClingoLoadError(Exception):
    pass


class Runner:

    def __init__(self, clingo_stats, input_path=None):
        self.clingo_stats=clingo_stats
        self.files = []
        self.parser = None
        if input_path:
            self.add(input_path)

    def add(self, input_path):
        """ Add input file or all IDP files from input directory. """
        if os.path.isfile(input_path):
            logging.debug(f"Adding file '{input_path}'.")
            if not input_path.endswith(".idp"):
                logging.warning(f"Input file '{input_path}' has not the expected '.idp' extension.")
            self.files.append(input_path)
        elif os.path.isdir(input_path):
            input_files = [os.path.join(input_path, x) for x in sorted(os.listdir(input_path)) if x.endswith(".idp")]
            logging.info(f"Adding the following input file(s): " + ', '.join(input_files))
            self.files.extend(input_files)
        else:
            logging.warning(f"Cannot find specified input file or directory '{input_path}'.")

    def run(self, execute=True, inference_task=None, output=True, output_path=None):
        """ Run FOLASP on file(s). Optionally execute parsed file with ASP solver and convert output back to FOL. """
        if not self.files:
            logging.warning("Nothing to do. Provide (an) IDP input file(s) to run FOLASP.")
            return
        if output and output_path:
            try:
                os.makedirs(os.path.dirname(output_path), exist_ok=True)
            except FileNotFoundError:
                pass
        if DEBUG:
            os.makedirs("./log", exist_ok=True)

        for fi in self.files:
            print(fi)
            idp = self.read(fi)                             # read idp input from file
            if not idp:
                logging.warning("Nothing to do. Got an empty file.")
                continue

            logging.info('Parsing FOL program.')
            self.parser = Folasp()                          # convert idp input to asp input
            lst = self.parser.parse_fol(idp)
            if not lst:
                logging.warning('Nothing to do. Got a file with no parsable IDP block(s).')
                continue
            logging.info('Converting FOL program to ASP.')
            asp = self.parser.convert_fol_to_asp(inference_task=inference_task)
            if DEBUG:
                f = f"./log/{os.path.splitext(os.path.basename(fi))[0]}"
                self.write(asp, f"{f}.lp.in")
                idp = self.parser.output_fol()
                self.write(idp, f"{f}.idp.in")

            if not execute:                                 # write asp input to file (if not executing)
                if output:
                    if output_path:
                        if output_path.endswith(".lp"):
                            fo = output_path
                        elif os.path.isdir(output_path):
                            fo = os.path.join(output_path, f"{os.path.splitext(os.path.basename(fi))[0]}.lp")
                        else:
                            logging.info("Provide a directory or '.lp' file as output path "
                                         "in order to save parsed ASP program when not executing.")
                            continue
                    else:
                        fo = f"{os.path.splitext(fi)[0]}.lp"
                    self.write(asp, fo)
                continue
            logging.debug('Executing ASP program...')
            is_sat, asp_out = self.execute_clingo(asp,self.clingo_stats)  # execute asp solver
            if DEBUG:
                self.write(asp_out, f"{f}.lp.out")

            print("result:", is_sat)
            if is_sat.unknown:
                raise ValueError(f'"{is_sat}" should be SAT or UNSAT.')
            elif is_sat.unsatisfiable:
                #exit(0) # UNSAT
                continue

            logging.info('Converting ASP output back to FOL.')
            idp_out = self.parser.convert_asp_output_to_fol(asp_out)    # convert asp output to idp output
            if DEBUG:
                self.write(idp_out, f"{f}.idp.out")

            if output:                                                  # write idp output to file
                if output_path:
                    if os.path.isdir(output_path):
                        fo = os.path.basename(fi).split('.')
                        fo = os.path.join(output_path, '.'.join(fo[:-1]) + '_out.' + fo[-1])
                    else:
                        fo = output_path
                else:
                    fo = fi.split('.')
                    fo = '.'.join(fo[:-1]) + '_out.' + fo[-1]
                if not fo.endswith(".idp"):
                    logging.warning(f"Output file '{fo}' does not have the expected '.idp' extension.")

                self.write(idp_out, fo)

    @staticmethod
    def print_clingo_stats(stats,k1,k2):
        for k3 in stats[k1][k2]:
            print(k3,":",stats[k1][k2][k3])

    @staticmethod
    def execute_clingo(asp,clingo_stats):
        from clingo.control import Control
        from io import StringIO
        import uuid

        def raise_unsat(arg):
            raise ClingoUnsatError("Unsat")

        def raise_loaderror(arg):
            raise ClingoLoadError(arg)

        path = f'/tmp/{uuid.uuid4()}.lp'
        Runner.write(asp, path)

        logging.info("Running Clingo.")
        ctl = Control()
        try:
            ctl.load(path)
        except RuntimeError as e:
            raise_loaderror(e)
        ctl.ground([("base", [])])

        old_stdout = sys.stdout
        sys.stdout = model = StringIO()
        sat = ctl.solve(on_model=print) #, on_core=raise_unsat)
        sys.stdout = old_stdout  # Reset stdout.
        if clingo_stats==1:
            Runner.print_clingo_stats(ctl.statistics,"problem","generator")
            Runner.print_clingo_stats(ctl.statistics,"problem","lp")
            Runner.print_clingo_stats(ctl.statistics,"solving","solvers")
            Runner.print_clingo_stats(ctl.statistics,"summary","models")
        elif clingo_stats==2:
            from json import dumps
            print(dumps(ctl.statistics, sort_keys=True,indent=2, separators=(',', ': ')))

        Runner.remove(path)
        return sat, model.getvalue()

    @staticmethod
    def read(path):
        """ Read file. """
        try:
            with open(path, 'r') as f:
                logging.info(f"Reading file '{path}'.")
                s = f.read()
        except IOError:
            logging.error('Could not find or open file.')
            return None
        return s

    @staticmethod
    def write(s, path):
        """ Write file. """
        logging.info(f"Writing file '{path}'.")
        if os.path.isfile(path):
            logging.warning("Output file will be overwritten.")
        with open(path, 'w') as f:
            f.write(s)

    @staticmethod
    def remove(path):
        """ Remove file. """
        logging.info(f"Removing file '{path}'.")
        try:
            os.remove(path)
        except FileNotFoundError:
            logging.warning(f"Cannot remove {path}, because path does not exist.")
            pass


def set_loglevel(loglevel):
    global DEBUG
    loglevel = getattr(logging, loglevel.upper())
    logging.basicConfig(level=loglevel)
    DEBUG = (loglevel <= getattr(logging, "DEBUG"))


def main():
    argp = argparse.ArgumentParser(description='FOLASP: convert FO(.) to ASP')
    argp.add_argument('-i', '--input', type=str, required=True,
                      help='input file (ending in \'.idp\') or folder')
    argp.add_argument('-o', '--output', type=str, default=False,  nargs='?',
                      help='output file (ending in \'.idp\') or folder')
    argp.add_argument('-x', '--execute', default=False,  action='store_true',
                      help='run ASP solver on converted file')
    argp.add_argument('--inference-task', type=str, default='auto',  nargs='+', metavar='TASK',
                      help='choose inference task to perform: "auto", "modelexpand" or "minimize" (default "auto") '
                           '- auto (runs minimize if term block provided else modelexpand), '
                           '- modelexpand'
                           '- minimize (provide term, otherwise last one in file is used)')
    #todo argp.add_argument('--nb-models', type=int, default=1,
    #                  help='provide number of models to compute if executing solver, default "1"')
    argp.add_argument('--log', type=str, default='info',
                      help='set logging level (error, warning, info or debug)')
    argp.add_argument('--clingo-stats', type=int, default=0, help='set the level of detail for printing the statistics from clingo (0,1,2)')
    args = argp.parse_args()
    if args.output:                                         # output
        output = True
    else:
        output = (args.output == None)
    set_loglevel(args.log)                                       # logging
    if len(args.inference_task) == 1:
        args.inference_task = args.inference_task[0]
    r = Runner(args.clingo_stats)
    r.add(args.input)
    r.run(execute=args.execute, inference_task=args.inference_task,
          output=output, output_path=args.output)


if __name__ == '__main__':
    main()
