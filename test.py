# Testing pipeline for the FOLASP project.
# Authors: Simon Vandevelde, Kylian Van Dessel

from folasp import Runner, Folasp, LexError, YaccError, ClingoLoadError, ClingoUnsatError, set_loglevel
import glob
import sys
import pprint
pp = pprint.PrettyPrinter(indent=4)
import os

# Configure
file_location = 'testing/'
ignore_files_ending_with = ['_broken', '_unsupported', '_out']  # ignore files ending with
redirect_output_to_file = 'log.txt'  # Redirect output to given file
set_log_level_to = 'info'  # Values: None, error, warning, info, debug
print_succes = False  # also print files without errors

# Set logging (built in FOLASP)
if set_log_level_to:
    set_loglevel(set_log_level_to)

# Redirect output and error messages to log file.
if redirect_output_to_file:
    old_stdout = sys.stdout
    old_stderr = sys.stderr
    sys.stdout = open(redirect_output_to_file, 'w')
    sys.stderr = sys.stdout

# Keep track of errors
lex_errors = {}
yacc_errors = {}
clingo_errors = {}
unsat_errors = {}
other_errors = {}
succes = []

# Select files
files = set(glob.glob(f'{file_location}**/*.idp', recursive=True)) - \
    set().union(*map(lambda x: set(glob.glob(f'{file_location}**/*{x}.idp', recursive=True)), ignore_files_ending_with))

# Run FOLASP on selection of files, collect errors
for i, f in enumerate(files):
    print('_'*60 + f'\nTest {i+1} of {len(files)+1}...')
    try:
        r = Runner()
        r.files.append(f)
        r.run(execute=True, output_path='tmp/')
    except LexError as e:
        lex_errors[f] = str(e)
    except YaccError as e:
        yacc_errors[f] = str(e)
    except ClingoLoadError as e:
        clingo_errors[f] = str(e)
    except ClingoUnsatError as e:
        if '_unsat' not in f:
            unsat_errors[f] = str(e)
    except Exception as e:
        other_errors[f] = str(e)
    else:
        succes.append(f)

# Reset previous stdout setting.
if redirect_output_to_file:
    sys.stdout = old_stdout
    sys.stderr = old_stderr

# Communicate pipeline results
if (len(lex_errors) == len(yacc_errors) == len(clingo_errors)
        == len(unsat_errors) == len(other_errors) == 0):
    print('Pipeline ran succesfully without errors.')
    if print_succes and succes != []:
        pp.pprint(succes)
    sys.exit(0)
else:
    print('Pipeline failed!\n')
    nb_succ = (len(files) - len(lex_errors) - len(yacc_errors)
               - len(clingo_errors) - len(unsat_errors) - len(other_errors))
    print(f'{nb_succ} out of {len(files)} files succeeded')

    if lex_errors != {}:
        print('The following LEX errors occurred:')
        pp.pprint(lex_errors)
        print('\n')

    if yacc_errors != {}:
        print('The following YACC errors occurred:')
        pp.pprint(yacc_errors)
        print('\n')

    if clingo_errors != {}:
        print('The following CLINGO errors occurred:')
        pp.pprint(clingo_errors)
        print('\n')

    if unsat_errors != {}:
        print('The following files returned UNSAT:')
        pp.pprint(unsat_errors)
        print('\n')

    if other_errors != {}:
        print('The following files returned other errors:')
        pp.pprint(other_errors)
        print('\n')

    if print_succes and succes != []:
        print('The following files ran successfully:')
        pp.pprint(succes)

    sys.exit(1)
