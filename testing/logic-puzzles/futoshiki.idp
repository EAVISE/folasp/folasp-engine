/*  
 * futoshiki.idp
 * kylian van dessel
 */

vocabulary V {
    type Row isa int
    type Column isa int
    type Value isa int
    GreaterThan(Row, Column, Row, Column)

    Given(Row, Column, Value)
    Solution(Row, Column): Value
} 

structure S: V {
    Row = { 0..3 }
    Column = { 0..3 }  
    Value = { 1..4 }
    GreaterThan = { 1,1,2,1; 1,2,2,2; 3,2,3,1; }

    Given = { 0,2,4; 3,3,1; }
}

theory T: V {
    // given values are part of solution
    !r[Row] c[Column] n[Value]: Given(r, c, n) => Solution(r, c) = n.
    
    // each row/column contains all possible numbers (exactly once)
    !r[Row] n[Value]: ?c[Column]: Solution(r,c)=n.
    !c[Column] n[Value]: ?r[Row]: Solution(r,c)=n.
    
    // greater than relation between cells should hold
	!r[Row] c[Column] r2[Row] c2[Column]: GreaterThan(r, c, r2, c2) => Solution(r, c) > Solution(r2, c2).
}

procedure main() {
    stdoptions.nbmodels = 3	
    printmodels(modelexpand(T,S))
}	
