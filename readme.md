# FOLASP

Model expansion engine for FO(·) with back-end ASP solver Clingo 


## Description

FOLASP is a model  expansion  engine  for  FO(·). It  uses  the  syntax  of  the IDP system  for  its  input  and  output,  and  uses Clingo(Gebser et al. 2019) as back-end ASP solver. 

The engine covers almost all  language  constructs  supported  by IDP,  except  for  symbol overloading, chained (in)equalities and constructed types. 
Other limitations are:

* Support for arithmetic is limited, not all constructs will lead to a valid transformation yet. 
* A logical theory cannot contain free variables and each quantified variable should have an explicit type annotation, e.g., !x[T].

The engine supports model expansion inference and optimization inference.
 

## Getting started

### Installation

```bash
git clone https://gitlab.com/EAVISE/folasp/folasp-engine
cd folasp-engine
pip3 install -r requirements.txt
```

### Executing program

Usage: folasp.py [-h] --i INPUT_FILE [-o [OUTPUT_FILE]] [--execute] [--inference-task INFERENCE_TASK [ARG ...]] [--log LOGGING_LEVEL] [--clingo-stats CLINGO_STATS]

Try running FOLASP on example.idp:

```python
 python3 folasp.py -i example.idp -o --execute 

```

Note that if the option -x or --execute is not given, the transformed ".lp" file will be outputed, instead of a ".idp" file containing the solution to the model expansion (or optimization) problem converted back to FO(·).

Also see "Help".


## Help

Run for help:

```python
 python3 folasp.py -h
```


## Authors

Kylian Van Dessel

Jo Devriendt

Joost Vennekens

